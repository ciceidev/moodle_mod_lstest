<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../config.php');
require_once('lib.php');

global $DB, $USER;

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}

require_login($course->id);

// Make sure this is a legitimate posting
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:taketest', $context);

if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error("Learning styles test ID was incorrect");
}

// Delete stored user records for this test instance
$DB->delete_records('lstest_user_answers', array('lstestid' => $lstest->id, 'userid' => $USER->id));
$DB->delete_records('lstest_user_scores', array('lstestid' => $lstest->id, 'userid' => $USER->id));

redirect($_SERVER["HTTP_REFERER"], get_string("changessaved"), 1);
exit;
?>