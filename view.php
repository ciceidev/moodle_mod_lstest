<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $DB, $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../config.php');
require_once('locallib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

$actualstudent = optional_param('actualstudent', false, PARAM_BOOL);
$coursemean = optional_param('coursemean', false, PARAM_BOOL);
$activitymean = optional_param('activitymean', false, PARAM_BOOL);
$totalmean = optional_param('totalmean', false, PARAM_BOOL);
$zoom = optional_param('zoom', false, PARAM_BOOL);
$writemean = optional_param('writemean', false, PARAM_BOOL);

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}
if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error('Course module is incorrect');
}

require_login($course->id);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:viewtest', $context);

add_to_log($course->id, "lstest", "view", "view.php?id=$cm->id", "$lstest->id", $cm->id);

$PAGE->set_title(format_string($lstest->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add(get_string('modulename', 'lstest'));
$PAGE->navbar->add(format_string($lstest->name));
$PAGE->set_url('/mod/lstest/view.php', array('id' => $id));

echo $OUTPUT->header();

$test = $DB->get_record('lstest_tests', array('id' => $lstest->testsid));

if ($test) {

    lstest_print_result_menu($course->id, $id, 'view');

    $answers = $DB->get_records('lstest_answers', array('testsid' => $test->id), 'id asc');
    $styles = $DB->get_records('lstest_styles', array('testsid' => $test->id), 'id asc');
    $onestyle = current($styles);

    $numscore = $DB->count_records('lstest_user_scores', array('lstestid' => $lstest->id, 'userid' => $USER->id, 'stylesid' => $onestyle->id));

    // Graph & results table
    if ($numscore > 0) {

        if (empty($CFG->gdversion)) {
            echo "(" . get_string('gdneed') . ")";
        }
        else {
            $datestamp = userdate(lstest_completed_date($USER->id, $lstest->id));
            echo $OUTPUT->heading(get_string('testcompleted', 'lstest', $datestamp));

            if (!$actualstudent && !$coursemean && !$activitymean && !$totalmean) {
                $actualstudent = true;
                $activitymean = true;
                $coursemean = false;
                $totalmean = false;
            }

            // Graph
            lstest_print_graphic($cm->id, $USER->id, $actualstudent, $coursemean, $activitymean, $totalmean, $zoom, $writemean);
            lstest_print_graphic_selector("view.php?id=$id", $id, $course->id, $USER->id, $actualstudent, $coursemean, $activitymean, $totalmean, $zoom, $writemean);

            // Current student scores
            $studentscores = lstest_student_scores($lstest->id, $USER->id);
            // Calculate means for this test
            $scores = lstest_mean_scores($lstest->id, $test->id, $course->id);
            // Print table of results
            lstest_print_result_table($test->id, $studentscores, $scores['activity'], $scores['course'], $scores['all']);
        }
    }

    // Print test
    if (( $numscore > 0) && ($lstest->redoallowed)) {
        echo $OUTPUT->heading(get_string('redotest', 'lstest'), 3);
        ?>
        <center>
            <form name="form" method="post" action="clean.php">
                <input type="submit" value=<?php print_string("continue") ?>>
                <input type="hidden" name="id" value="<?php echo $id ?>">
            </form>
        </center>
        <?php
    }

    if ($numscore == 0) {
        echo '<br>';
        echo $OUTPUT->box_start();
        echo '<center>' . format_text($lstest->intro, $lstest->introformat) . '</center>';
        echo $OUTPUT->box_end();

        $items = $DB->get_records('lstest_items', array('testsid' => $test->id), 'id asc');

        $table->head[0] = '';
        $table->align[0] = 'left';
        $table->width = '100%';
        $table->size = array('', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

        $table->cellpadding = 15;
        ?>

        <script>
            function checkform() {

                var error=false;

        <?php
        if (!$test->notansweredquestion) {
            if ($test->multipleanswer) { //respuesta multiple
                $firstanswer = current($answers);
                foreach ($items as $item) {
                    $stranswer = "document.form.answer";
                    $condition = "(" . $stranswer . $item->id . $firstanswer->id . ".checked ";
                    foreach ($answers as $answer) {
                        if ($answer->id != $firstanswer->id) {
                            $condition .= " || " . $stranswer . $item->id . $answer->id . ".checked ";
                        }
                    }
                    $condition .= ")";
                    echo "  if (!" . $condition . ") error=true;\n";
                }
            } else {
                foreach ($items as $item) {
                    $stranswer = "document.form.answer" . $item->id;
                    $condition = "(" . $stranswer . "[0].checked ";
                    $answersnum = count($answers);
                    for ($i = 1; $i < $answersnum; $i++) {
                        $condition .= " || " . $stranswer . "[" . $i . "].checked ";
                    }
                    $condition .= ")";
                    echo "  if (!" . $condition . ") error=true;\n";
                }
            }
        }
        ?>

        if (error) {
        alert("<?php print_string("questionsnotanswered", "lstest") ?>");
        } else {
        document.form.submit();
        }
        }
        </script>

        <form name="form" method="post" action="save.php">
            <center>
                <br><br>

        <?php
        foreach ($answers as $answer) {
            $table->head[$answer->id] = $answer->name;
            $table->align[$answer->id] = "center";
        }

        $counter = 1;
        foreach ($items as $item) {
            $table->data[$item->id][0] = "<b>$counter.- $item->question</b>";
            $counter++;
            $stritemid = "itemid" . $item->id;
            echo "<input type=hidden name=$stritemid value=$item->id>";
            $stranswer = "answer" . $item->id;
            foreach ($answers as $answer) {
                if ($test->multipleanswer) {
                    $stranswer = "answer" . $item->id . $answer->id;
                    $table->data[$item->id][$answer->id] = "<input type=checkbox name=$stranswer>";
                } else {
                    $table->data[$item->id][$answer->id] = "<input type=radio name=$stranswer  value=$answer->id >";
                }
            }
        }
        lstest_print_table($table);
        ?>

                <br>
                <br>
                <input type=button value=<?php print_string("continue") ?> onClick=checkform();>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <br>
                <br>
            </center>

        </form>

        <?php
    }
}
else {
    print_string('testnotavailable', 'lstest');
}
echo $OUTPUT->footer();
?>
