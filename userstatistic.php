<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $DB, $PAGE, $OUTPUT, $CFG;

require_once('../../config.php');
require_once('locallib.php');

$id = optional_param('id', 0, PARAM_INT);        // Course Module ID
$userid = optional_param('userid', 0, PARAM_INT);        // Course Module ID
$actualstudent = optional_param('actualstudent', false, PARAM_BOOL);
$coursemean = optional_param('coursemean', false, PARAM_BOOL);
$categorymean = optional_param('categorymean', false, PARAM_BOOL);
$totalmean = optional_param('totalmean', false, PARAM_BOOL);
$zoom = optional_param('zoom', false, PARAM_BOOL);
$writemean = optional_param('writemean', false, PARAM_BOOL);
$orderby = optional_param('orderby', 'items', PARAM_ALPHA);

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}
if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error('Course module is incorrect');
}

require_login($course->id);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:viewstatistics', $context);

add_to_log($course->id, "lstest", "view", "view.php?id=$cm->id", "$lstest->id");

$PAGE->set_title(format_string($lstest->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add(get_string('modulename', 'lstest'));
$PAGE->navbar->add(format_string($lstest->name));
$PAGE->set_url('/mod/lstest/userstatistic.php', array('id' => $id));

echo $OUTPUT->header();

lstest_print_result_menu($course->id, $id, 'userstatistic');

// Show link to generate an excel report file
if ($userid) {
    $excelparams = "lstestid=$lstest->id&userid=$userid";
} else {
    $excelparams = "lstestid=$lstest->id";
}
$reportstr = get_string('report', 'lstest');
$xlsicon = $OUTPUT->pix_url('f/xlsx');
echo "<center><a href=\"$CFG->wwwroot/mod/lstest/report.php?$excelparams\" target=\"_blank\" class=\"btn\">
            <img src=\"$xlsicon\" class=\"icon\" alt=\"xls\" />
            $reportstr
            </a></center>";

// Show results for student
if ($userid) {

    echo '<BR>';
    $user = $DB->get_record('user', array('id' => $userid));
    echo $OUTPUT->heading(get_string('seestudent', 'lstest', "$user->firstname $user->lastname"));

    $scores = lstest_mean_scores($lstest->id, $lstest->testsid, $course->id);
    $studentscores = lstest_student_scores($lstest->id, $userid);

    if (!$actualstudent && !$coursemean && !$categorymean && !$totalmean) {
        $actualstudent = true;
        $coursemean = true;
        $categorymean = false;
        $totalmean = false;
    }

    lstest_print_graphic($cm->id, $userid, $actualstudent, $coursemean, $categorymean, $totalmean, $zoom, $writemean);

    lstest_print_graphic_selector("userstatistic.php?id=$id&userid=$userid", $id, $course->id, $userid, $actualstudent, $coursemean, $categorymean, $totalmean, $zoom, $writemean, $orderby);

    lstest_print_result_table($lstest->testsid, $studentscores, $scores['activity'], $scores['course'], $scores['all']);

    echo $OUTPUT->heading_with_help(get_string('seeanswers', 'lstest', "$user->firstname $user->lastname"), 'seeanswers', 'lstest');

    lstest_print_answer_table($lstest->id, $lstest->testsid, $id, $userid, $actualstudent, $coursemean, $categorymean, $totalmean, $zoom, $writemean, $orderby);
}

// Show table of students that have been completed the test
$table = new html_table();
$table->head[0] = get_string('selectstudent', 'lstest');
$table->align[0] = 'center';

$students = lstest_activity_students($lstest->id);
if ($students) {
    $counter = 0;
    foreach ($students as $studentid) {
        $user = $DB->get_record('user', array('id' => $studentid));
        $table->data[$counter++][0] = "<A HREF=userstatistic.php?id=$id&userid=$studentid>$user->firstname $user->lastname</A><BR>";
    }
}
echo '<BR><CENTER>';
echo html_writer::table($table);
echo '</CENTER><BR>';
echo $OUTPUT->footer();
?>


