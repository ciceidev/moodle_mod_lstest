<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;

require_once($CFG->dirroot . '/course/moodleform_mod.php');

class mod_lstest_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $DB, $OUTPUT;

        $mform = & $this->_form;

        /// Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

        /// Adding the standard "name" field
        $mform->addElement('text', 'name', get_string('name'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        /// Adding the required "intro" field to hold the description of the instance
        $this->add_intro_editor(true);

        // Test selection is avalaible in activity creation
        // Test selection is avalaible in activity modification only if there isn't test results yet

        // Activity is already created, check if there are scores stored
        if (isset($this->_cm)) {
            $courseid = $this->_cm->course;
            $lstestid = $this->_cm->instance;
            $scores = $DB->count_records('lstest_user_scores', array('lstestid' => $lstestid));
        }
        // Activity is on creation process
        else {
            $courseid = required_param('course', PARAM_INT);
            $lstestid = NULL;
            $scores = 0;
        }

        // If there are scores stored, show only current test to select (can't change test)
        if (isset($scores) && $scores > 0) {
            $lstest = $DB->get_record('lstest', array('id' => $lstestid));
            $tests = $DB->get_record('lstest_tests', array('id'=>  $lstest->testsid));
            $tests = array($tests);
        }
        // If not, load all avalaible tests for select
        else {
            $siteid = SITEID;
            $select = "courseid = '$courseid' OR courseid = '$siteid'";
            $tests = $DB->get_records_select('lstest_tests', $select, null, 'id desc');
        }

        $coursestr = get_string('course', 'moodle');
        $sitestr = get_string('site', 'moodle');

        if ($tests) {
            $options = array();
            foreach ($tests as $test) {
                $teststr =  "$test->name - $test->lang ";
                $teststr .= $test->courseid ? $coursestr : $sitestr;
                $options[$test->id] = $teststr;
            }
            $mform->addElement('select', 'testsid', get_string('modulename', 'lstest'), $options);
            $mform->addRule('testsid', get_string('required'), 'required', null, 'client');
        }
        else
        {
            $mform->addElement('static', 'label1', get_string('modulename', 'lstest'), get_string('errornotest', 'lstest'));
        }

        // Link to test editor for teachers
        $mform->addElement('html', "<br/><center><a href=\"$CFG->wwwroot/mod/lstest/editor/settings.php?course=$courseid\"><img src=\"" . $OUTPUT->pix_url('i/edit') . "\" class=\"icon\" alt=\"edit\" />Abrir editor de tests</a></center><br/>");

        $options = array(get_string('no'), get_string('yes'));
        $mform->addElement('select', 'redoallowed', get_string('redoallowed', 'lstest'), $options);
        $mform->addHelpButton('redoallowed', 'redoallowed', 'lstest');

        // add standard elements, common to all modules
        $this->standard_coursemodule_elements();

        // add standard buttons, common to all modules
        $this->add_action_buttons();
    }

}

?>
