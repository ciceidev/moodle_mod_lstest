<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['addinganswers'] = 'Respuestas del test';
$string['addinganswers_help'] = 'Aquí se debe indicar cuales son las posibles respuestas del alumno ante las preguntas del test. Por ejemplo, para el test CHAEA son: Poco de acuerdo y Muy de acuerdo.';
$string['addingitems'] = 'Preguntas del test';
$string['addingitems_help'] = 'Aquí se debe indicar los enunciados de las preguntas del test y el estilo de aprendizaje al que pertenece cada una de ellas. Además, para cada pregunta se debe indicar la puntuación obtenida según se marque o no cada una de las respuestas posibles. Así, para cada una de las respuestas, aparecen debajo dos valores: el de la izquierda para indicar la puntuación obtenida si no se elige dicha respuesta y el de la derecha para la puntuación obtenida en caso de que sí se elija dicha respuesta.';
$string['addinglevels'] = 'Niveles de pertenencia a los estilos';
$string['addinglevels_help'] = 'Aquí se debe indicar cuales son los posibles niveles de pertenencia de un alumno a un estilo de aprendizaje concreto. Por ejemplo, para el CHAEA son: Muy baja, Baja, Moderada, Alta y Muy alta. Deben introducirse los nombres en orden de menor a mayor pertenencia, es decir, arriba del todo el que signifique menor pertenencia y abajo del todo el que signifique mayor pertenencia.';
$string['addingstyles'] = 'Estilos de aprendizaje';
$string['addingstyles_help'] = 'Aquí se debe indicar el nombre de los estilos de aprendizaje que se van a usar en el test. Por ejemplo, en el test CHAEA son: Activo, Reflexivo, Teórico y Pragmático.';
$string['addingthresholds'] = 'Umbrales de pertenencia a los estilos';
$string['addingthresholds_help'] = 'Aquí se debe indicar, para cada uno de los estilos de aprendizaje que contiene el test, qué puntuación obtenida se corresponque con qué nivel de pertenencia. Por ejemplo, para el estilo Activo del test CHAEA: una puntuación de 0-6 (ambos inclusive) corresponde al nivel Muy bajo, 7-8 corresponde al nivel Bajo, 9-12 al nivel Moderado, 13-14 al Alto y 15-20 al Muy alto.';
$string['addnewlstest'] = 'Nuevo test de estilos de aprendizaje';
$string['addnewlstest_help'] = 'Haciendo clic aquí podrá definir un nuevo test de estilos de aprendizaje. Se le ira guiando a través de sucesivas pantallas en las que se le solicitará toda la información relativa al nuevo test.';
$string['actualstudent'] = 'Alumno actual';
$string['actualuser'] = 'Usuario actual';
$string['answer'] = 'Respuesta';
$string['answername'] = 'Respuesta {$a}';
$string['answersnum'] = 'Número de respuestas';
$string['answersnum_help'] = 'Aquí se debe indicar cuál es el número de respuestas posibles para cada pregunta del test. Por ejemplo, para el test CHAEA son 2: Poco de acuerdo y Bastante de acuerdo.';
$string['available'] = 'Se encuentra disponible';
$string['available_help'] = 'Aquí se debe indicar si el test estará o no disponible para que lo usen los profesores en los cursos.';
$string['baduse'] = 'Se ha hecho un mal uso de la página';
$string['activitymean'] = 'Media de la actividad';
$string['cannotdelete'] = 'No se puede borrar el test porque hay actividades con puntuación que usan ese test';
$string['checked'] = 'Elegido';
$string['course'] = 'Curso';
$string['coursemean'] = 'Media del curso';
$string['delete'] = 'Borrar';
$string['delete_help'] = 'Haga clic en el test que quiera eliminar.';
$string['deleteconfirm'] = '¿Quiere eliminar definitivamente el test {$a}?';
$string['edit'] = 'Editar';
$string['edit_help'] = 'Haga clic en el test que quiera editar, bien sea para modificar algo o para consultar alguna información relativa al test.';
$string['editinganswers'] = 'Editando respuestas del test';
$string['editingitems'] = 'Editando preguntas del test';
$string['editinglevels'] = 'Editando niveles de pertenencia';
$string['editingstyles'] = 'Editando estilos de aprendizaje';
$string['editingstyletest'] = 'Editando test de estilos de aprendizaje';
$string['editingstyletest_help'] = 'Los test de estilos de aprendizaje permiten clasificar a los alumnos en varios de niveles de pertenencia a varios estilos de aprendizaje. Con esa información se podría personalizar un curso en base a los distintos niveles de pertenencia de los alumnos a los estilos. Los tests de estilos de aprendizaje vienen identificados por un nombre y un idioma, es decir, no se puede crear un test con el mismo nombre e idioma que uno existente.';
$string['editingthresholds'] = 'Editando umbrales de pertenencia';
$string['errornotest'] = 'No hay tests disponibles!';
$string['export'] = 'Exportar';
$string['export_help'] = 'Haga clic en un test para exportar toda su información a un único fichero en formato XML.';
$string['fillallfields'] = 'Debes rellenar todos los campos';
$string['forlevel'] = 'Para el nivel {$a}';
$string['forstyle'] = 'Para el estilo {$a}';
$string['forstylepredominance'] = 'Alumnos en los que predomina el estilo {$a}';
$string['havemadethetest'] = 'Han hecho el test';
$string['helpstyles'] = 'Ayuda sobre los test de estilos de aprendizaje';
$string['hideshow'] = 'Ocultar/Mostrar';
$string['hideshow_help'] = "Aquí se debe indicar si el test estará o no disponible para que lo usen los profesores en los cursos.";
$string['importtest'] = 'Importar test de estilos de aprendizaje';
$string['importtest_help'] = 'Para importar un test de estilos de aprendizaje a partir de un fichero XML haga clic en examinar y seleccione el fichero y dele al botón "Importar test de estilos de aprendizaje".';
$string['inthecourse'] = 'En el curso';
$string['intheactivity'] = 'En la actividad';
$string['inmoodle'] = 'En la plataforma';
$string['introduction'] = 'Introducción';
$string['introeg'] = 'El propósito de este test es identificar cuál es tu estilo de aprendizaje para poder recomendarte las actividades más adecuadas a tu perfil';
$string['introtext'] = 'Texto de introducción';
$string['itemanswers'] = 'Respuestas de los alumnos la pregunta {$a->number}: {$a->statement} (estilo {$a->style})';
$string['itemanswers_help'] = 'En esta tabla se muestra, para una pregunta concreta, si cada alumno del curso marco cada una de las respuestas posibles. Pinchando en el nombre de cualquiera de los alumnos se accede a la parte de "Estadísticas de alumnos" con el alumno escogido como protagonista.';
$string['itemnumber'] = 'Pregunta {$a}';
$string['items'] = 'Enunciado y estilo de las preguntas';
$string['itemsnotanswered'] = 'Debes introducir los enunciados de todas las preguntas';
$string['itemsnum'] = 'Número de preguntas';
$string['itemsnum_help'] = 'Aquí se debe indicar cuál es el número de preguntas que posee el test de estilos de aprendizaje. Por ejemplo, el test CHAEA posee 80 preguntas.';
$string['itemstatistics'] = 'A continuación se muestra la distribución de las respuestas dadas a cada pregunta';
$string['itemstatistics_help'] = 'En la siguiente tabla se muestra, para cada pregunta, su estilo y el número de alumnos que marcó cada una de las posibles respuestas, tanto en el curso como en la categoría y en la plataforma. En un principio las preguntas aparecen en el orden en que fueron definidas. Si se pincha en la opción "Ordenar por estilos" las preguntas aparecerán ordenadas por el estilo al que pertenecen y aparecerá, a su vez, la opción "Ordenar por preguntas" para volver a la vista inicial. Pinchando en el enunciado de cualquiera de las preguntas se accede a una información más detallada sobre esa pregunta, en concreto a lo que contestó cada alumno.';
$string['langnotfound'] = 'Lenguaje no instalado {$a}';
$string['levelname'] = 'Nombre del nivel {$a}';
$string['levelsnum'] = 'Número de niveles de pertenencia';
$string['levelsnum_help'] = 'Aqui se debe indicar cuál es el número de niveles de pertenencia que se usan en el test que se va a crear. Los niveles de pertenencia se usan para evaluar en que medida una persona posee cada uno de los estilos de aprendizaje. Por ejemplo, para el test CHAEA, hay 5 niveles de pertenencia: Muy baja, Baja, Moderada, Alta y Muy alta.';
$string['maxandminresults'] = 'Puntuaciones máximas y mínimas';
$string['maxandminresults_help'] = 'Nótese que, por ejemplo las puntuaciones máximas para todos los estilos dentro del curso, no tienen que pertenecer, en absoluto, al mismo usuario.';
$string['maxscore'] = 'Puntuación máxima';
$string['maxthreshold'] = 'max';
$string['minscore'] = 'Puntuación mínima';
$string['minthreshold'] = 'min';
$string['modulename'] = 'Test de estilos de aprendizaje';
$string['modulenameplural'] = 'Tests de estilos de aprendizaje';
$string['multipleanswer'] = 'Respuesta múltiple';
$string['multipleanswer_help'] = 'Aquí se debe indicar si las preguntas del test son de respuesta única o múltiple.';
$string['mustbeadmin'] = '¡Debes ser administrador!';
$string['mustbeadminorteacher'] = '¡Debes ser administrador o profesor!';
$string['namenotanswered'] = 'El campo nombre esta vacío';
$string['nofile'] = 'No has rellenado el nombre del fichero o es incorrecto';
$string['notansweredquestion'] = 'Preguntas sin contestar';
$string['notansweredquestion_help'] = 'Aquí se debe indicar si se permiten preguntas sin contestar o si para dar el test por finalizado se debe responder a todas.';
$string['notchecked'] = 'Sin elegir';
$string['numberofstudents'] = 'Número de alumnos que han realizado el test';
$string['pertenency'] = 'Su pertenencia';
$string['predominance'] = 'A continuación se muestra el número de alumnos en los que predomina cada estilo';
$string['predominance_help'] = 'En la primera columna de la tabla se muestra el número de alumnos inscritos en el curso. En la segunda aparece el número total y el porcentaje(respecto al número de alumnos inscritos) de alumnos que han hecho el test. Y en las siguientes columnas se muestra, para cada uno de los estilos, en qué número y porcentaje (ahora respecto a numero de alumnos que han hecho el test) de alumnos es predominante dicho estilo. Nótese que en un alumno pueden se predominantes varios o todos los estilos al mismo tiempo.';
$string['levelreport'] = 'Nivel {$a}';
$string['question'] = 'Pregunta';
$string['questionsnotanswered'] = 'Debes responder a todas las preguntas';
$string['openeditor'] = 'Abrir editor de tests';
$string['orderbyitems'] = 'Ordenar por preguntas';
$string['orderbystyles'] = 'Ordenar por estilos';
$string['redoallowed'] = 'Se puede rehacer';
$string['redoallowed_help'] = 'Aquí se debe indicar si los usuarios pueden hacer el test más de una vez. Cuando esta opción esté activada los usuarios podran hacer el test siempre que quieran. Cuando esté desactivada sólo podrán realizar el test los alumnos que nunca lo hayan hecho. Sin embargo, los usuarios siempre podrán consultar sus resultados.';
$string['redographic'] = 'Redibujar gráfica';
$string['redographic_help'] = 'Al pulsar este botón se redibuja la gráfica con los resultados del alumno. A la hora de dibujar se tendrá en cuenta qué gráficas se quieren mostrar(alumno actual, media del curso, media de la categoría y/o media de la plataforma), si se desea hacer zoom (ampliar la imagen un poco) y si las medias (del curso, categoría y plataforma) deben escribirse o no en la gráfica. Cuando esta desmarcada la opción "Escribir las medias" se escriben en negro los valores del alumno actual. Y cuando está marcada dicha opción los valores de cada media toman el mismo valor de su gráfica.';
$string['redotest'] = 'Si lo desea puede volver a realizarlo a continuación para actualizar sus valores de pertenencia a los distintos estilos de aprendizaje.';
$string['report'] = 'Generar informe en Excel';
$string['score'] = 'Su puntuación';
$string['scorereport'] = 'Puntuación {$a}';
$string['seeanswers'] = 'Respuestas al test del alumno {$a}';
$string['seeanswers_help'] = 'En la siguiente tabla se muestra, para cada pregunta, su estilo y si el alumno marcó cada una de las posibles respuestas. En un principio las preguntas aparecen en el orden en que fueron definidas. Si se pincha en la opción "Ordenar por estilos" las preguntas aparecerán ordenadas por el estilo al que pertenecen y aparecerá, a su vez, la opción "Ordenar por preguntas" para volver a la vista inicial. Pinchando en el enunciado de cualquiera de las preguntas se accede a la parte de "Estadísticas de preguntas", con la pregunta seleccionada como protagonista.';
$string['seestudents'] = 'Estadísticas de alumnos';
$string['seestudents_help'] = 'Aqui podrás seleccionar un alumno para ver la puntuación obtenida en los estilos del test por dicho alumno(comparada con la media del curso, de la categoría y de la plataforma) y su respuesta a cada una de las preguntas.';
$string['seestudent'] = 'Resultados del alumno {$a}';
$string['seestyleanswers'] = 'Respuestas al test ordenadas por estilos del alumno {$a}';
$string['seeitemstatistic'] = 'Estadísticas de preguntas';
$string['seeitemstatistic_help'] = 'Aquí se muestra qué número de usuarios marcó cada respuesta para cada pregunta, dentro del curso, de la categoría y de la totalidad de usuarios de la plataforma. Si se escoge una pregunta concreta se mostrará lo que contestarón todos los alumnos del curso a dicha pregunta.';
$string['seestylestatistic'] = 'Estadísticas de estilos';
$string['seestylestatistic_help'] = 'Estadísticas de estilos';
$string['seeteststatistic'] = 'Estadísticas del test';
$string['seeteststatistic_help'] = 'Aquí se muestra el número de usuarios que han hecho el test y las puntuaciones medias, máximas y mínimas obtenidas en cada estilo. Todo ello para los alumnos del curso, de la categoría y la totalidad de usuarios de la plataforma.';
$string['selectstudent'] = 'Selecciona un alumno de los que han completado el test';
$string['sitedoesntexist'] = '¡¡ El sitio no existe !!';
$string['student'] = 'Alumno';
$string['studentsinthecourse'] = 'Alumnos en el curso';
$string['style'] = 'Estilo';
$string['stylename'] = 'Nombre del estilo {$a}';
$string['stylesnum'] = 'Número de estilos';
$string['stylesnum_help'] = 'Aquí se debe indicar cuál es el número de estilos de aprendizaje que se usan en el test que se va a crear. Los estilos de aprendizaje se usan para clasificar a personas según la forma en que aprenden mejor. Por ejemplo, para el test CHAEA, hay 4 estilos de aprendizaje: Activo, Reflexivo, Teórico y Pragmático.';
$string['testcompleted'] = 'Usted ya realizó este test el {$a}. A continuación se muestran sus resultados';
$string['testexists'] = 'Ya existe un test con ese nombre y ese idioma';
$string['testnotavailable'] = 'El test no se encuentra disponible.';
$string['testresults'] = 'Resultados obtenidos en el test';
$string['totalmean'] = 'Media de la plataforma';
$string['viewtest'] = 'Mi test';
$string['withoutanswers'] = 'El test no contiene posibles respuestas';
$string['withoutitems'] = 'El test no contiene preguntas';
$string['withoutlang'] = 'Debe indicarse el idioma del test';
$string['withoutlevels'] = 'El test no contiene niveles de pertenencia';
$string['withoutname'] = 'El test no tiene nombre';
$string['withoutscores'] = 'El test no contiene puntuaciones';
$string['withoutstyles'] = 'El test no contiene estilos';
$string['withoutthresholds'] = 'El test no contiene umbrales de pertenencia a los distintos estilos';
$string['writemeans'] = 'Escribir las medias';
$string['youcannotedit'] = '¡¡ No tienes permisos para editar los tests de estilos de aprendizaje !!';
$string['youcannotchange'] = '¡¡ Debes ser administrador para cambiar los tests de estilos de aprendizaje !!';
$string['zoom'] = 'Zoom';
// For Moodle 2
$string['pluginname'] = $string['modulename'];
$string['pluginadministration'] = 'Administración de LSTest';
$string['lstest:addinstance'] = 'Añadir test de Estilos de Aprendizaje';
$string['lstest:editor'] = 'Usar el editor de tests';
$string['lstest:viewstatistics'] = 'Ver estadísticas de tests';
$string['lstest:viewtest'] = 'Ver tests';
$string['lstest:taketest'] = 'Hacer tests';
?>
