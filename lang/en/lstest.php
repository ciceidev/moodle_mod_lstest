<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['addinganswers'] = 'Test\'s answers';
$string['addinganswers_help'] = "Here you must write the possible answers to test's questions. For example, the answers of CHAEA test are: I don't agree and I agree.";
$string['addingitems'] = 'Test\'s question';
$string['addingitems_help'] = "Here you must fill the statements and learning styles of each question. You must select, for each question too, the obtained score if each answers is or isn't checked. So, for each answer, appears two values below: the left one to select the obtained score if the current answer isn't checked and the rigth one to select the obtained score if the current answer is checked.";
$string['addinglevels'] = 'Styles pertenency levels';
$string['addinglevels_help'] = 'Here you must say the possible pertenency levels to a learning style. For example, CHAEA test uses: . The names must be introduced in order from lower to higher pertenency, that is, above the lowest pertenency and below the higher one.';
$string['addingstyles'] = 'Learning styles';
$string['addingstyles_help'] = "Here you must say the name of the test's learning styles. For example, CHAEA test uses four styles.";
$string['addingthresholds'] = 'Styles pertenency thresholds';
$string['addingthresholds_help'] = 'Here you must say, for each test learning styles, what score obtained in the test correspond to each pertenency level. For example, for  ACTIVE style of CHAEA test: a score of 0-6 (both included) corresponds to VERY LOW level, 7-8 to LOW level, 9-12 to MODERATE, 13-14 to HIGH and 15-20 to VERY HIGH.';
$string['addnewlstest'] = 'New learning styles test';
$string['addnewlstest_help'] = 'Click here to define a new learning styles test. You will be guided throught several screens in which all information related to the new test will be asked.';
$string['actualstudent'] = 'Current student';
$string['actualuser'] = 'Current user';
$string['answer'] = 'Answer';
$string['answername'] = 'Answer {$a}';
$string['answersnum'] = 'Number of answers';
$string['answersnum_help'] = "Here you must write the number of possible answers to test's questions. For example, the number of answer for CHAEA test is 2: I don't agree and I agree.";
$string['available'] = 'It\'s available';
$string['available_help'] = 'Here you must say if this test will be available to be used by teachers in their courses.';
$string['baduse'] = 'You haven\'t used this page correctly';
$string['activitymean'] = 'Activity mean';
$string['cannotdelete'] = 'Test cannot be deleted because there are activities that use the test that have students scores';
$string['checked'] = 'Checked';
$string['course'] = 'Course';
$string['coursemean'] = 'Course mean';
$string['delete'] = 'Delete';
$string['delete_help'] = 'Click in the test to delete.';
$string['deleteconfirm'] = 'Do you really want to delete the {$a} test?';
$string['edit'] = 'Edit';
$string['edit_help'] = 'Click in the test which you want to edit, modify or consult.';
$string['editinganswers'] = 'Editing test answers';
$string['editingitems'] = 'Editing test questions';
$string['editinglevels'] = 'Editing pertenency levels';
$string['editingstyles'] = 'Editing learning styles';
$string['editingstyletest'] = 'Editing learning styles test';
$string['editingstyletest_help'] = "Learning styles tests allow to clasify students in some learning styles pertenency levels. With this information a course could be personalized to the students's styles pertenency levels. Learning styles tests are identified by a name and a language. A test with the same name and language than an existing one cannot be created.";
$string['editingthresholds'] = 'Editing pertenency thresholds';
$string['errornotest'] = 'There aren\'t available tests!';
$string['export'] = 'Export';
$string['export_help'] = 'Click in a test to export all its information to a XML file.';
$string['fillallfields'] = 'You must fill all fields';
$string['forlevel'] = 'For level {$a}';
$string['forstyle'] = 'For style {$a}';
$string['forstylepredominance'] = 'Students with predominant {$a} style';
$string['havemadethetest'] = 'Have made the test';
$string['helpstyles'] = 'Help about learning styles test';
$string['hideshow'] = 'Hide/Show';
$string['hideshow_help'] = "Here you must say if the test will be available to teachers to use it. The tests with a shut eye can be used by teachers in their courses, while whose with an open eye can't be used. Click in a test's eye to change its state.";
$string['importtest'] = 'Import learning styles test';
$string['importtest_help'] = 'To import a learning styles test from a XML file write the file name (or click in browse and choose a file) and click in "Import learning styles test" button.';
$string['inthecourse'] = 'In the course';
$string['intheactivity'] = 'In the activity';
$string['inmoodle'] = 'In the site';
$string['introduction'] = 'Intro';
$string['introeg'] = 'The target of this test is to know your predominant learning styles and so offer to you the most suitable activities';
$string['introtext'] = 'Introduction text';
$string['itemanswers'] = 'Students\'s answers to question  {$a->number}: {$a->statement} (style {$a->style)}';
$string['itemanswers_help'] = 'This table shows, to a concrete question, if each student that have made the test marked each of possible answers. If you click in the name of a student you will be redirected to "Students statistics" zone with selected student as leading.';
$string['itemnumber'] = 'Question {$a}';
$string['items'] = 'Questions statement and style';
$string['itemsnotanswered'] = 'You must fill all question statements';
$string['itemsnum'] = 'Question number';
$string['itemsnum_help'] = 'Here you must choose the number of questions of the test. For example, CHAEA test has 80 questions (20 for each style).';
$string['itemstatistics'] = 'Next the answers distribution of each question is given';
$string['itemstatistics_help'] = 'In the next table appears for each question its style and the number of students that marked each of possible answers, in the course, category and site. At the beginning, questions appears in the order they were defined. If you click in "Order by styles" option, questions will be ordered by its style and will appear the option "Order by questions" to return to the initial view. If you click in the statement of a question you will view a more detailed information related to question. In particular the answers of each student to that question.';
$string['langnotfound'] = 'Language not installed {$a}';
$string['levelname'] = 'Name of level {$a}';
$string['levelsnum'] = 'Pertenency levels number';
$string['levelsnum_help'] = 'Here you must say how many pertenency levels uses the test that is being created. Pertenency levels are used to evaluate how much a person has each learning style. For example, CHAEA test uses 5 pertenency levels.';
$string['maxandminresults'] = 'Max. and min. scores';
$string['maxandminresults_help'] = 'Note that, for example, higher scores in a course for all styles can be of different students for each style.';
$string['maxscore'] = 'Max. score';
$string['maxthreshold'] = 'max';
$string['minscore'] = 'Min. score';
$string['minthreshold'] = 'min';
$string['modulename'] = 'Learning styles test';
$string['modulenameplural'] = 'Learning styles tests';
$string['multipleanswer'] = 'Multiple answer';
$string['multipleanswer_help'] = 'Here you must say if more than one answers can be gived to a question.';
$string['mustbeadmin'] = 'You must be admin!';
$string['mustbeadminorteacher'] = 'You must be admin or teacher!';
$string['namenotanswered'] = 'Name field is empty';
$string['nofile'] = 'You have not filled the file name or it\'s incorrect';
$string['notansweredquestion'] = 'There are questions not answered';
$string['notansweredquestion_help'] = 'Here you must say if users are allowed to left questions without answer.';
$string['notchecked'] = 'Not checked';
$string['numberofstudents'] = 'Number of students which have made the test';
$string['pertenency'] = 'Obtained pertenency';
$string['predominance'] = 'Next number of students in which each style is predominant is given';
$string['predominance_help'] = 'The first table column shows the number of students in the course. In the second total number and percentage (regarding students in the course) of students that have made the test appears. And the next columns show, for each style, the total number and percentage (regarding students that have made the test) of students in which the style is predominant. Note that, in a students, several or even all styles can be predominant.';
$string['levelreport'] = 'Level for {$a}';
$string['question'] = 'Question';
$string['questionsnotanswered'] = 'You must answer all question';
$string['openeditor'] = 'Open tests editor';
$string['orderbyitems'] = 'Order by questions';
$string['orderbystyles'] = 'Order by styles';
$string['redoallowed'] = 'Users can redo';
$string['redoallowed_help'] = "Here you must say if users can do the test more than one time. With this option activated users can do the test all times they want. And with this option deactivated users only can do the test if they haven't done it before. However, users can always consult their results.";
$string['redographic'] = 'Redraw graphic';
$string['redographic_help'] = "Click here to redraw the graphic with student's answers. When redrawing, graphics to show (current student, course mean, category mean and/or site mean) are considered. It's considered too if you want to zoom and if mean scores (course, category, site) must be written. When the option \"Write means\" is unmarked, current student's scores are written in black colour. But when marked all scores are written in the same colour as corresponding graphic.";
$string['redotest'] = 'If you want you can redo the test to update your pertenency levels in each learning style';
$string['report'] = 'Generate excel report';
$string['score'] = 'Obtained score';
$string['scorereport'] = 'Score for {$a}';
$string['seeanswers'] = 'Answers of the student {$a}';
$string['seeanswers_help'] = 'In the next table appears, for each question, its style and if student marked each one of the possible answers. At the beginning, questions appears in the order they were defined. If you click in "Order by styles" option, questions will be ordered by its style and will appear the option "Order by questions" to return to the initial view. If you click in the statement of a question you will be redirected to "Question statistics" zone with selected questions as leading.';
$string['seestudents'] = 'Students statistics';
$string['seestudents_help'] = 'Here you will be able to select a student to see his test scores obtained (compared with course, category and site mean) and his answers to each question.';
$string['seestudent'] = 'Student {$a} results';
$string['seestyleanswers'] = 'Student {$a} answers ordered by styles';
$string['seeitemstatistic'] = 'Question statistics';
$string['seeitemstatistic_help'] = 'Here you can view how many students marked each answer to each question, in the course, category and site. If you select a particular question the answers to this question of all students will be shown.';
$string['seestylestatistic'] = 'Style statistics';
$string['seestylestatistic_help'] = "Here you can consult what styles predominate in each student, such as the number of students in which each style predominates. A learning style predominates in a student when his pertenency level to that style isn't superated by his pertenency level in remaining styles.";
$string['seeteststatistic'] = 'Test statistics';
$string['seeteststatistic_help'] = 'Here you can view the number of students that have made the test, such us the higher, lower and mean scores obtained in each style. All this information is given for course, category and site students.';
$string['selectstudent'] = 'Choose a students between those who have filled the test';
$string['sitedoesntexist'] = 'The site doesn\'t exists!';
$string['student'] = 'Student';
$string['studentsinthecourse'] = 'Students in the course';
$string['style'] = 'Style';
$string['stylename'] = 'Style {$a} name';
$string['stylesnum'] = 'Number of styles';
$string['stylesnum_help'] = 'Here you must say how many learning styles are used in the test which is being created. Learning styles test are used to clasify people by the way they learn best. For example, CHAEA test uses 4 styles.';
$string['testcompleted'] = 'You filled this test on {$a}. Next your results are given';
$string['testexists'] = 'A test with same name and language already exists';
$string['testnotavailable'] = 'Test isn\'t available.';
$string['testresults'] = 'Test obtained results';
$string['totalmean'] = 'Site mean';
$string['viewtest'] = 'My test';
$string['withoutanswers'] = 'Test don\'t have possible answers';
$string['withoutitems'] = 'Test don\'t have questions';
$string['withoutlang'] = 'You must give test languaje';
$string['withoutlevels'] = 'Test don\'t have pertenency levels';
$string['withoutname'] = 'Test don\'t have name';
$string['withoutscores'] = 'Test don\'t have scores ';
$string['withoutstyles'] = 'Test don\'t have styles';
$string['withoutthresholds'] = 'Test don\'t have pertenency thresholds';
$string['writemeans'] = 'Write means';
$string['youcannotedit'] = '¡¡ You don\'t have permissions to edit leraning style tests !!';
$string['youcannotchange'] = 'You must be admin to change learning style tests!';
$string['zoom'] = 'Zoom';
// For Moodle 2
$string['pluginname'] = $string['modulename'];
$string['pluginadministration'] = 'LSTest administration';
$string['lstest:addinstance'] = 'Add Learning Style test';
$string['lstest:editor'] = 'Use tests editor';
$string['lstest:viewstatistics'] = 'View tests statistics';
$string['lstest:viewtest'] = 'View tests';
$string['lstest:taketest'] = 'Take tests';
?>
