<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// TODO: Use http://www.php.net/manual/en/book.simplexml.php or another XML tool to create XML file

global $DB;

require_once('../../../config.php');
require_once('../locallib.php');
require($CFG->libdir . '/filelib.php');

$testid = required_param('testid', PARAM_INT);
$courseid = optional_param('course', SITEID, PARAM_INT);

require_login($courseid);
lstest_editor_check_access($courseid);

require_sesskey();

if (!lstest_can_edit_test($testid)) {
    error(get_string('youcannotchange', 'lstest'));
}

$test = $DB->get_record('lstest_tests', array('id' => $testid));

$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n";

$content .= "<TEST>\n\n";

$content .= "    <NAME>$test->name</NAME>\n\n";
$content .= "    <LANG>$test->lang</LANG>\n\n";
$content .= "    <AVAILABLE>$test->available</AVAILABLE>\n\n";
$content .= "    <REDOALLOWED>$test->redoallowed</REDOALLOWED>\n\n";
$content .= "    <MULTIPLEANSWER>$test->multipleanswer</MULTIPLEANSWER>\n\n";
$content .= "    <NOTANSWEREDQUESTION>$test->notansweredquestion</NOTANSWEREDQUESTION>\n\n";

$styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
$content .= "    <STYLES>\n";
$order = 1;
foreach ($styles as $style) {
    $content .= "        <STYLE>$style->name</STYLE>\n";
    $stylestoorder[$style->id] = $order++;
}
$content .= "    </STYLES>\n\n";

$levels = $DB->get_records('lstest_levels', array('testsid' => $testid), 'id asc');
$content .= "    <LEVELS>\n";
$order = 1;
foreach ($levels as $level) {
    $content .= "        <LEVEL>$level->name</LEVEL>\n";
    $levelstoorder[$level->id] = $order++;
}
$content .= "    </LEVELS>\n\n";


$answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
$content .= "    <ANSWERS>\n";
$order = 1;
foreach ($answers as $answer) {
    $content .= "        <ANSWER>$answer->name</ANSWER>\n";
    $answerstoorder[$answer->id] = $order++;
}
$content .= "    </ANSWERS>\n\n";


$items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
$content .= "    <ITEMS>\n";
$order = 1;
foreach ($items as $item) {
    $content .= "\n        <ITEM>\n";
    $content .= "            <ITEMSTYLE>" . $stylestoorder[$item->stylesid] . "</ITEMSTYLE>\n";
    $content .= "            <QUESTION>$item->question</QUESTION>\n";
    $content .= "        </ITEM>\n";
    $itemstoorder[$item->id] = $order++;
}
$content .= "    </ITEMS>\n\n";

$content .= "    <SCORES>\n";
foreach ($items as $item) {
    foreach ($answers as $answer) {
        $score = $DB->get_record('lstest_scores', array('itemsid' => $item->id, 'answersid' => $answer->id));
        $content .= "\n        <SCORE>\n";
        $content .= "            <SCOREITEM>" . $itemstoorder[$item->id] . "</SCOREITEM>\n";
        $content .= "            <SCOREANSWER>" . $answerstoorder[$answer->id] . "</SCOREANSWER>\n";
        $content .= "            <NOCHECKEDSCORE>" . $score->nocheckedscore . "</NOCHECKEDSCORE>\n";
        $content .= "            <CHECKEDSCORE>" . $score->checkedscore . "</CHECKEDSCORE>\n";
        $content .= "        </SCORE>\n";
    }
}
$content .= "\n    </SCORES>\n\n";

$content .= "    <THRESHOLDS>\n";
foreach ($styles as $style) {
    foreach ($levels as $level) {
        $threshold = $DB->get_record('lstest_thresholds', array('stylesid' => $style->id, 'levelsid' => $level->id));
        $content .= "\n        <THRESHOLD>\n";
        $content .= "            <THRESHOLDSTYLE>" . $stylestoorder[$style->id] . "</THRESHOLDSTYLE>\n";
        $content .= "            <THRESHOLDLEVEL>" . $levelstoorder[$level->id] . "</THRESHOLDLEVEL>\n";
        $content .= "            <INFTHRESHOLD>$threshold->infthreshold</INFTHRESHOLD>\n";
        $content .= "            <SUPTHRESHOLD>$threshold->supthreshold</SUPTHRESHOLD>\n";
        $content .= "        </THRESHOLD>\n";
    }
}
$content .= "\n    </THRESHOLDS>\n\n";



$content .= "</TEST>\n";

$filename = tempnam("/tmp", "lstest");
$file = fopen($filename, "w");
fwrite($file, $content);
fclose($file);

send_file("$filename", "test.xml");
?>
