<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG, $USER, $DB, $OUTPUT, $PAGE;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/settings.php', array('course' => $courseid));

echo $OUTPUT->header();

$modpath = "$CFG->wwwroot/mod/lstest";

$strname = get_string('name');
$strlang = get_string('language');
$strhide = get_string('hide');
$strshow = get_string('show');
$strhideshow = get_string('hideshow', 'lstest');
$strdelete = get_string('delete');
$stredit = get_string('edit');
$strexport = get_string('export', 'lstest');

$languages = get_string_manager()->get_list_of_translations();

// Get only tests of this course if current user is a teacher
$select = $courseid != SITEID && !is_siteadmin() ? "courseid = '$courseid'" : '';
$tests = $DB->get_records_select('lstest_tests', $select, null, 'id asc');

$table = new html_table();
$table->head = array(
    $strname,
    $strlang,
    get_string('course'),
    $strhideshow . $OUTPUT->help_icon('hideshow', 'lstest'),
    $strdelete . $OUTPUT->help_icon('delete', 'lstest'),
    $stredit . $OUTPUT->help_icon('edit', 'lstest'),
    $strexport . $OUTPUT->help_icon('export', 'lstest')
);
$table->align = array('CENTER', 'CENTER', 'CENTER', 'CENTER', 'CENTER', 'CENTER', 'CENTER');
$table->width = '80%';

if ($tests) {

    $pixpath = "$CFG->wwwroot/mod/lstest/pix";

    foreach ($tests as $test) {

        $lang = isset($languages[$test->lang]) ? $languages[$test->lang] : get_string('langnotfound', 'lstest', $test->lang);

        if ($test->courseid != SITEID) {
            $course = $DB->get_record('course', array('id' => $test->courseid));
            if ($course) {
                $course = "<a href=\"$CFG->wwwroot/course/view.php?id=$course->id\">$course->fullname</a>";
            }
            else {
                $course = get_string('childcoursenotfound', 'moodle');
            }
        }
        else {
            $course = get_string('site', 'moodle');
        }

        if ($test->available) {
            $available = "<a href=$modpath/editor/change.php?action=hide&testsid=$test->id&course=$courseid&sesskey=$USER->sesskey title=\"$strhide\">" .
                    "<img src=\"$pixpath/hide.gif\" align=\"absmiddle\" height=16 width=16 border=0></a>";
        } else {
            $available = "<a href=$modpath/editor/change.php?action=show&testsid=$test->id&course=$courseid&sesskey=$USER->sesskey title=\"$strshow\">" .
                    "<img src=\"$pixpath/show.gif\" align=\"absmiddle\" height=16 width=16 border=0></a>";
        }

        $delete = "<a href=$modpath/editor/change.php?action=delete&testsid=$test->id&course=$courseid&sesskey=$USER->sesskey title=\"$strdelete\">" .
                "<img src=\"$pixpath/delete.gif\" align=\"absmiddle\" height=16 width=16 border=0></a>";

        $edit = "<a href=$modpath/editor/test.php?testsid=$test->id&course=$courseid&sesskey=$USER->sesskey title=\"$stredit\">" .
                "<img src=\"$pixpath/edit.gif\" align=\"absmiddle\" height=16 width=16 border=0></a>";

        $export = "<a target=\"_blank\" href=$modpath/editor/export.php?testid=$test->id&course=$courseid&sesskey=$USER->sesskey title=\"$strexport\">" .
                "<img src=\"$pixpath/export.gif\" align=\"absmiddle\" height=16 width=16 border=0></a>";

        $table->data[] = array(
            $test->name,
            $lang,
            $course,
            $available,
            $delete,
            $edit,
            $export
        );
    }
}
else {
    $table->data[] = array(get_string('errornotest', 'lstest'));
}

echo '</br><center>';
echo html_writer::table($table);

echo "</center><center><table style=\"width: 80%; text-align: center;\"><tr>";
$straddnewstyle = get_string("addnewlstest", "lstest");
echo  "<br><td><FORM name=\"form\" method=\"post\" action=$modpath/editor/test.php>";
echo  "<input type=\"submit\" value=\"$straddnewstyle\">";
echo $OUTPUT->help_icon('addnewlstest', 'lstest');
echo  "<input type=\"hidden\" name=\"sesskey\" value=\"$USER->sesskey\">";
echo  "<input type=\"hidden\" name=\"course\" value=\"$courseid\">";
echo  "</FORM></td>";

$strimporttest = get_string("importtest", "lstest");
echo  "<td><FORM enctype=\"multipart/form-data\" method=\"post\" action=$modpath/editor/import.php>";
echo  "<input type=hidden name=MAX_FILE_SIZE value=100000>";
echo  "<input name=userfile type=file>";
echo  "</br><input type=\"submit\" value=\"$strimporttest\">";
echo $OUTPUT->help_icon('importtest', 'lstest');
echo  "<input type=\"hidden\" name=\"sesskey\" value=\"$USER->sesskey\">";
echo  "<input type=\"hidden\" name=\"course\" value=\"$courseid\">";
echo  "</FORM></td>";
echo  "</tr></table></center>";

echo $OUTPUT->footer();

?>
