<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);
require_sesskey();

$test = lstest_get_test_submitted();
$styles = lstest_get_styles_submitted($test->stylesnum);
$levels = lstest_get_levels_submitted($test->levelsnum);
$answers = lstest_get_answers_submitted($test->answersnum);

$items = lstest_get_items($test->itemsnum, $test->answersnum, $test->id);

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/items.php');

echo $OUTPUT->header();

$pageheading = get_string('addingitems', 'lstest');
echo $OUTPUT->heading_with_help($pageheading, 'addingitems', 'lstest');
echo $OUTPUT->box_start();
?>

<script>
function checkform() {
    var error=false;

    <?php
    for ($i=1; $i<=$test->itemsnum; $i++) {
        $stritemstatement = "document.form.question".$i.".value";
        echo "  if (!".$stritemstatement.") error=true;\n";
    }
    ?>

    if (error) {
        alert("<?php print_string("itemsnotanswered", "lstest") ?>");
    } else {
        document.form.submit();
    }
}
</script>

<FORM name="form" method="post" action="<?php echo "$CFG->wwwroot/mod/lstest/editor/thresholds.php"; ?>">
<CENTER>
<TABLE cellpadding=5>

<TR valign=top>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <?php
    for($i=1; $i<=$test->answersnum; $i++) {
        $stranswer = "answer".$i;
        echo "<TD colspan=2 align=center><B>";
        p($answers->$stranswer);
        echo "</B></TD>";
    }
    ?>
</TR>

<TR valign=top>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <?php
    for($i=1; $i<=$test->answersnum; $i++) {
        echo "<TD align=center>";
        print_string("notchecked", "lstest");
        echo "</TD>";
        echo "<TD align=center>";
        print_string("checked", "lstest");
        echo "</TD>";
    }
    ?>
</TR>

<?php

if ( !empty($test->id)) {
    $onestyles = $DB->get_records('lstest_styles', array('testsid' => $test->id), 'id asc');
    $i = 1;
    foreach ($onestyles as $onestyle) {
        $styleids[$i++] = $onestyle->id;
    }
    $i = 1;
    foreach ($styles as $stylename) {
        $options[$styleids[$i++]] = $stylename;
    }
} else {
    $i = 1;
    foreach ($styles as  $stylename) {
        $options[$i++] = $stylename;
    }
}

for ($i=-10; $i<=10; $i++) {
    $scoreoptions[$i] = "$i";
}


for ($i=1; $i<=$test->itemsnum; $i++) {
    $strquestion = "question".$i;
    $strstylesid = "stylesid".$i;
    ?>

    <TR valign=top>
        <td>
            <P><B><?php  print_string('itemnumber', "lstest", $i) ?>:</B></P>
        </td>
        <TD align=right>
            <textarea name="<?php p($strquestion) ?>" rows=2 cols=50><?php p($items->$strquestion) ?></textarea>
        </TD>
        <TD>
            <?php echo html_writer::select($options, $strstylesid, $items->$strstylesid); ?>
        </TD>
        <?php
        for ($j=1; $j<=$test->answersnum; $j++) {
            $strnocheckedscore = "nocheckedscore".$i.$j;
            $strcheckedscore = "checkedscore".$i.$j;

            ?>
            <TD align=center>
                <?php echo html_writer::select($scoreoptions, $strnocheckedscore, $items->$strnocheckedscore); ?>
            </TD>
            <TD align=center>
                <?php echo html_writer::select($scoreoptions, $strcheckedscore, $items->$strcheckedscore); ?>
            </TD>
            <?php
        }
        ?>
    </TR>

    <?php
}
?>

</TABLE>
<br>

<input type=button value=<?php print_string("continue") ?> onClick=checkform()>

<?php
lstest_submit_test($test);
lstest_submit_styles($test->stylesnum, $styles);
lstest_submit_levels($test->levelsnum, $levels);
lstest_submit_answers($test->answersnum, $answers);
?>

<input type="hidden" name="sesskey" value="<?php p("$USER->sesskey") ?>">

</CENTER>
</FORM>

<?php
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>
