<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);
require_sesskey();

$test = lstest_get_test_submitted();
$styles = lstest_get_styles_submitted($test->stylesnum);
$levels = lstest_get_levels_submitted($test->levelsnum);
$answers = lstest_get_answers_submitted($test->answersnum);
$items = lstest_get_items_submitted($test->itemsnum, $test->answersnum);

$thresholds = lstest_get_thresholds($test->stylesnum, $test->levelsnum, $test->id);

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/thresholds.php');

echo $OUTPUT->header();

$pageheading = get_string('addingthresholds', 'lstest');
echo $OUTPUT->heading_with_help($pageheading, 'addingthresholds', 'lstest');
echo $OUTPUT->box_start();

if (!empty($test->id)) {
    $url_params = "?action=update&testsid=$test->id";
} else {
    $url_params = "?action=add&testsid=0";
}
?>

<FORM name="form" method="post" action="<?php echo "$CFG->wwwroot/mod/lstest/editor/change.php$url_params"; ?>">
<CENTER>

<?php

$options = array();
for ($i=-$test->itemsnum; $i<=$test->itemsnum; $i++) {
    $options[$i] = "$i";
}

for ($i=1; $i<=$test->stylesnum; $i++) {

    $strstyle = "style".$i;
    $pageheading = get_string('forstyle', "lstest", $styles->$strstyle);

    echo $OUTPUT->heading($pageheading);
    echo $OUTPUT->box_start();
?>
    <TABLE cellpadding=4>
    <TR align=center>
        <TD></TD>
        <TD> <?php p(get_string('minthreshold', "lstest")) ?> </TD>
        <TD>-</TD>
        <TD> <?php p(get_string('maxthreshold', "lstest")) ?> </TD>
    </TR>

<?php

    for ($j=1; $j<=$test->levelsnum; $j++) {

        $strlevel = "level".$j;
        $strinfthreshold = "infthreshold".$i.$j;
        $strsupthreshold = "supthreshold".$i.$j;

?>

        <TR>
            <TD align=right><P><B><?php  print_string('forlevel', "lstest", $levels->$strlevel) ?>:</B></P></TD>
            <TD>
            <?php echo html_writer::select($options, $strinfthreshold, $thresholds->$strinfthreshold); ?>
            </TD>
            <TD>-</TD>
            <TD>
            <?php echo html_writer::select($options, $strsupthreshold, $thresholds->$strsupthreshold); ?>
            </TD>
        </TR>

<?php

    }

?>

</TABLE>

<?php
    echo $OUTPUT->box_end();
}
?>

<br>
<INPUT type="submit" value="<?php  print_string("savechanges") ?>">

<?php
lstest_submit_test($test);
lstest_submit_styles($test->stylesnum, $styles);
lstest_submit_levels($test->levelsnum, $levels);
lstest_submit_answers($test->answersnum, $answers);
lstest_submit_items($test->itemsnum, $test->answersnum, $items);
?>

<input type="hidden" name="sesskey" value="<?php p("$USER->sesskey") ?>">

</CENTER>
</FORM>

<?php
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>
