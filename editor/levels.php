<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);
require_sesskey();

$test = lstest_get_test_submitted();
$styles = lstest_get_styles_submitted($test->stylesnum);

$levels = lstest_get_levels($test->levelsnum, $test->id);

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/levels.php');

echo $OUTPUT->header();

$pageheading = get_string('addinglevels', 'lstest');
echo $OUTPUT->heading_with_help($pageheading, 'addinglevels', 'lstest');
echo $OUTPUT->box_start();
?>

<script>
function checkform() {
    var error=false;

    <?php
    for ($i=1; $i<=$test->levelsnum; $i++) {
        $strlevel = "document.form.level".$i.".value";
        echo "  if (!".$strlevel.") error=true;\n";
    }
    ?>

    if (error) {
        alert("<?php print_string("fillallfields", "lstest") ?>");
    } else {
        document.form.submit();
    }
}
</script>

<FORM name="form" method="post" action="<?php echo "$CFG->wwwroot/mod/lstest/editor/answers.php"; ?>">
<CENTER>
<TABLE cellpadding=5>

<?php

for ($i=1; $i<=$test->levelsnum; $i++) {
    $strlevel = "level".$i;

?>

    <TR valign=top>
        <TD align=right><P><B><?php  print_string('levelname', "lstest", $i) ?>:</B></P></TD>
        <TD>
            <INPUT type="text" name="<?php p($strlevel) ?>" size=30 value="<?php  p($levels->$strlevel) ?>">
        </TD>
    </TR>

<?php
}
?>

</TABLE>
<br>

<input type=button value=<?php print_string("continue") ?> onClick=checkform();>

<?php
lstest_submit_test($test);
lstest_submit_styles($test->stylesnum, $styles);
?>

<input type="hidden" name="sesskey" value="<?php p("$USER->sesskey") ?>">

</CENTER>
</FORM>

<?php
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>
