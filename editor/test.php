<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);
require_sesskey();

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/test.php');

echo $OUTPUT->header();

$pageheading = get_string('editingstyletest', 'lstest');
echo $OUTPUT->heading_with_help($pageheading, 'editingstyletest', 'lstest');
echo $OUTPUT->box_start();

$lstestsid = optional_param('testsid', 0, PARAM_INT);
$test = lstest_get_test($lstestsid);
?>

<script>
    function checkform() {
        if (!document.form.name.value) {
            alert("<?php print_string("namenotanswered", "lstest") ?>");
        } else {
            document.form.submit();
        }
    }
</script>

<FORM name="form" method="post" action="<?php echo "$CFG->wwwroot/mod/lstest/editor/styles.php"; ?>">
    <CENTER>
        <TABLE cellpadding=5>
            <TR valign=top>
                <TD align=right><P><B><?php print_string("name") ?>:</B></P></TD>
                <TD>
                    <INPUT type="text" name="name" size=30 value="<?php p($test->name) ?>">
                </TD>
            </TR>

            <TR valign=top>
                <TD align=right><P><B><?php print_string("language") ?>:</B></P></TD>
                <TD>
                <?php echo html_writer::select(get_string_manager()->get_list_of_translations(), 'lang', $test->lang); ?>
                </TD>
            </TR>


            <tr valign=top>
                <td align=right><P><B><?php print_string("stylesnum", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                for ($i = 10; $i >= 2; $i--) {
                    $options[$i] = $i;
                }
                echo html_writer::select($options, 'stylesnum', $test->stylesnum);
                echo $OUTPUT->help_icon('stylesnum', 'lstest');
                ?>
                </td>
            </tr>

            <tr valign=top>
                <td align=right><P><B><?php print_string("levelsnum", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                for ($i = 10; $i >= 2; $i--) {
                    $options[$i] = $i;
                }
                echo html_writer::select($options, 'levelsnum', $test->levelsnum);
                echo $OUTPUT->help_icon('levelsnum', 'lstest');
                ?>
                </td>
            </tr>

            <tr valign=top>
                <td align=right><P><B><?php print_string("itemsnum", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                for ($i = 100; $i >= 2; $i--) {
                    $options[$i] = $i;
                }
                echo html_writer::select($options, 'itemsnum', $test->itemsnum);
                echo $OUTPUT->help_icon('itemsnum', 'lstest');
                ?>
                </td>
            </tr>

            <tr valign=top>
                <td align=right><P><B><?php print_string("answersnum", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                for ($i = 10; $i >= 2; $i--) {
                    $options[$i] = $i;
                }
                echo html_writer::select($options, 'answersnum', $test->answersnum);
                echo $OUTPUT->help_icon('answersnum', 'lstest');
                ?>
                </td>
            </tr>

            <tr>
                <td align=right><P><B><?php print_string("available", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                $options[0] = get_string("no");
                $options[1] = get_string("yes");
                echo html_writer::select($options, 'available', $test->available);
                echo $OUTPUT->help_icon('available', 'lstest');
                ?>
                </td>
            </tr>

            <tr>
                <td align=right><P><B><?php print_string("redoallowed", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                $options[0] = get_string("no");
                $options[1] = get_string("yes");
                echo html_writer::select($options, 'redoallowed', $test->redoallowed);
                echo $OUTPUT->help_icon('redoallowed', 'lstest');
                ?>
                </td>
            </tr>

            <tr>
                <td align=right><P><B><?php print_string("multipleanswer", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                $options[0] = get_string("no");
                $options[1] = get_string("yes");
                echo html_writer::select($options, 'multipleanswer', $test->multipleanswer);
                echo $OUTPUT->help_icon('multipleanswer', 'lstest');
                ?>
                </td>
            </tr>

            <tr>
                <td align=right><P><B><?php print_string("notansweredquestion", "lstest") ?>:</B></P></TD>
                <td>
                <?php
                $options = array();
                $options[0] = get_string("no");
                $options[1] = get_string("yes");
                echo html_writer::select($options, 'notansweredquestion', $test->notansweredquestion);
                echo $OUTPUT->help_icon('notansweredquestion', 'lstest');
                ?>
                </td>
            </tr>

        </TABLE>

        <input type="hidden" name="id"   value="<?php p("$test->id") ?>">
        <input type="hidden" name="sesskey" value="<?php p("$USER->sesskey") ?>">
        <input type="hidden" name="course" value="<?php p("$courseid") ?>">
        <br>
        <input type=button value=<?php print_string("continue") ?> onClick=checkform();>

    </CENTER>
</FORM>

<?php
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>
