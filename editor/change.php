<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG, $DB, $PAGE, $OUTPUT, $USER;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);

require_sesskey();

// Read action and test
$action = required_param('action', PARAM_ALPHANUM);
$testsid = required_param('testsid', PARAM_INT);

if($testsid && !lstest_can_edit_test($testsid)) {
    error(get_string('youcannotedit', 'lstest'));
}

// Get possible submitted test
$test = lstest_get_test_submitted();
if ($test) {
    $styles = lstest_get_styles_submitted($test->stylesnum);
    $levels = lstest_get_levels_submitted($test->levelsnum);
    $thresholds = lstest_get_thresholds_submitted($test->stylesnum, $test->levelsnum);
    $answers = lstest_get_answers_submitted($test->answersnum);
    $items = lstest_get_items_submitted($test->itemsnum, $test->answersnum);
}

// Avalaible actions
switch($action) {
    // Make a test avalaible
    case 'show': {
        $DB->set_field('lstest_tests', 'available', '1', array('id' => $testsid));
        break;
    }
    // Make a test unavalaible
    case 'hide': {
        $DB->set_field('lstest_tests', 'available', '0', array('id' => $testsid));
        break;
    }
    // Delete related info about a test
    case 'delete': {
        $confirm = optional_param('confirm', 'no', PARAM_ALPHANUM);

        // We can't delete a test if exist instances with user scores
        $lstests = $DB->get_records('lstest', array('testsid' => $testsid));
        if ($lstests) {
           foreach ($lstests as $lstest) {
               $userscores = $DB->get_records('lstest_user_scores', array('lstestid' => $lstest->id));
               if ($userscores) {
                   error(get_string('cannotdelete', 'lstest'), $_SERVER["HTTP_REFERER"]);
               }
           }
        }

        if ($confirm != 'yes') {
            lstest_editor_page_config($courseid);
            $PAGE->set_url('/mod/lstest/editor/change.php', array('action' => 'delete', 'testsid' => $testsid, 'course' => $courseid, 'sesskey' => $USER->sesskey));

            echo $OUTPUT->header();

            echo "<br><center>";
            $linkyes = new moodle_url("$CFG->wwwroot/mod/lstest/editor/change.php", array('action' => 'delete', 'testsid' => $testsid, 'course' => $courseid, 'confirm' => 'yes', 'sesskey' => $USER->sesskey));
            $linkno = new moodle_url("$CFG->wwwroot/mod/lstest/editor/settings.php", array('course' => $courseid));
            echo $OUTPUT->confirm(
                get_string('deleteconfirm', 'lstest', $DB->get_field('lstest_tests', 'name', array('id' => $testsid))),
                new single_button($linkyes, get_string('yes'), 'get'),
                new single_button($linkno, get_string('no'), 'get')
            );
            echo "</center><br>";

            echo $OUTPUT->footer();
            exit;
        }
        else {
            // Save these records to delete after thresholds and scores
            $styles = $DB->get_records('lstest_styles', array('testsid' => $testsid));
            $items = $DB->get_records('lstest_items', array('testsid' => $testsid));

            $DB->delete_records('lstest_tests', array('id' => $testsid));
            $DB->delete_records('lstest_styles', array('testsid' => $testsid));
            $DB->delete_records('lstest_levels', array('testsid' => $testsid));
            $DB->delete_records('lstest_items', array('testsid' => $testsid));
            $DB->delete_records('lstest_answers', array('testsid' => $testsid));

            if ($styles) {
                foreach ($styles as $style) {
                    $DB->delete_records('lstest_thresholds', array('stylesid' => $style->id));
                }
            }
            if ($items) {
                foreach ($items as $item) {
                    $DB->delete_records('lstest_scores', array('itemsid' => $item->id));
                }
            }
        }
        break;
    }
    // Update related info about a test
    // TODO: If we add new items to an existing test, they are not added to test
    case 'update': {
        $DB->update_record('lstest_tests', $test);

        $onestyles = $DB->get_records('lstest_styles', array('testsid' => $test->id), 'id asc');
        $i = 1;
        foreach ($onestyles as $onestyle) {
            $styleids[$i++] = $onestyle->id;
        }
        for ($i = 1; $i <= $test->stylesnum; $i++) {
            $strstyle = "style" . $i;
            $tablestyle->id = $styleids[$i];
            $tablestyle->name = $styles->$strstyle;
            $tablestyle->testsid = $test->id;
            $DB->update_record('lstest_styles', $tablestyle);
        }

        $onelevels = $DB->get_records('lstest_levels', array('testsid' => $test->id), 'id asc');
        $i = 1;
        foreach ($onelevels as $onelevel) {
            $levelids[$i++] = $onelevel->id;
        }
        for ($i = 1; $i <= $test->levelsnum; $i++) {
            $strlevel = "level" . $i;
            $tablelevel->id = $levelids[$i];
            $tablelevel->name = $levels->$strlevel;
            $tablelevel->testsid = $test->id;
            $DB->update_record('lstest_levels', $tablelevel);
        }

        for ($i = 1; $i <= $test->stylesnum; $i++) {
            for ($j = 1; $j <= $test->levelsnum; $j++) {
                $strinfthreshold = "infthreshold" . $i . $j;
                $strsupthreshold = "supthreshold" . $i . $j;
                $threshold = $DB->get_record('lstest_thresholds', array('stylesid' => $styleids[$i], 'levelsid' => $levelids[$j]));
                $tablethreshold->id = $threshold->id;
                $tablethreshold->stylesid = $styleids[$i];
                $tablethreshold->levelsid = $levelids[$j];
                $tablethreshold->infthreshold = $thresholds->$strinfthreshold;
                $tablethreshold->supthreshold = $thresholds->$strsupthreshold;
                $DB->update_record('lstest_thresholds', $tablethreshold);
            }
        }

        $oneanswers = $DB->get_records('lstest_answers', array('testsid' => $test->id), 'id asc');
        $i = 1;
        foreach ($oneanswers as $oneanswer) {
            $answerids[$i++] = $oneanswer->id;
        }
        for ($i = 1; $i <= $test->answersnum; $i++) {
            $stranswer = "answer" . $i;
            $answerid = "answerid" . $i;
            $tableanswer->id = $answerids[$i];
            $tableanswer->name = $answers->$stranswer;
            $tableanswer->testsid = $test->id;
            $DB->update_record('lstest_answers', $tableanswer);
        }

        $oneitems = $DB->get_records('lstest_items', array('testsid' => $test->id), 'id asc');
        $i = 1;
        foreach ($oneitems as $oneitem) {
            $itemids[$i++] = $oneitem->id;
        }

        for ($i = 1; $i <= $test->itemsnum; $i++) {
            $strquestion = "question" . $i;
            $strstylesid = "stylesid" . $i;
            $tableitem->id = $itemids[$i];
            $tableitem->testsid = $test->id;
            $tableitem->question = $items->$strquestion;
            $tableitem->stylesid = $items->$strstylesid;
            $DB->update_record('lstest_items', $tableitem);
            for ($j = 1; $j <= $test->answersnum; $j++) {
                $strnocheckedscore = "nocheckedscore" . $i . $j;
                $strcheckedscore = "checkedscore" . $i . $j;
                $score = $DB->get_record('lstest_scores', array('itemsid' => $itemids[$i], 'answersid' => $answerids[$j]));
                $tablescore->id = $score->id;
                $tablescore->itemsid = $itemids[$i];
                $tablescore->answersid = $answerids[$j];
                $tablescore->nocheckedscore = $items->$strnocheckedscore;
                $tablescore->checkedscore = $items->$strcheckedscore;
                $DB->update_record('lstest_scores', $tablescore);
            }
        }
        break;
    }
    // Add a new test
    case 'add': {
        $testid = $DB->insert_record('lstest_tests', $test);

        for ($i = 1; $i <= $test->stylesnum; $i++) {
            $strstyle = "style" . $i;
            $tablestyle->name = $styles->$strstyle;
            $tablestyle->testsid = $testid;
            $DB->insert_record('lstest_styles', $tablestyle);
        }

        for ($i = 1; $i <= $test->levelsnum; $i++) {
            $strlevel = "level" . $i;
            $tablelevel->name = $levels->$strlevel;
            $tablelevel->testsid = $testid;
            $DB->insert_record('lstest_levels', $tablelevel);
        }

        $onestyles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($onestyles as $onestyle) {
            $styleids[$i++] = $onestyle->id;
        }
        $onelevels = $DB->get_records('lstest_levels', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($onelevels as $onelevel) {
            $levelids[$i++] = $onelevel->id;
        }
        for ($i = 1; $i <= $test->stylesnum; $i++) {
            for ($j = 1; $j <= $test->levelsnum; $j++) {
                $strinfthreshold = "infthreshold" . $i . $j;
                $strsupthreshold = "supthreshold" . $i . $j;
                $tablethreshold->stylesid = $styleids[$i];
                $tablethreshold->levelsid = $levelids[$j];
                $tablethreshold->infthreshold = $thresholds->$strinfthreshold;
                $tablethreshold->supthreshold = $thresholds->$strsupthreshold;
                $DB->insert_record('lstest_thresholds', $tablethreshold);
            }
        }

        for ($i = 1; $i <= $test->answersnum; $i++) {
            $stranswer = "answer" . $i;
            $tableanswer->name = $answers->$stranswer;
            $tableanswer->testsid = $testid;
            $DB->insert_record('lstest_answers', $tableanswer);
        }

        $oneanswers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($oneanswers as $oneanswer) {
            $answerids[$i++] = $oneanswer->id;
        }

        for ($i = 1; $i <= $test->itemsnum; $i++) {
            $strquestion = "question" . $i;
            $strstylesid = "stylesid" . $i;
            $tableitem->testsid = $testid;
            $tableitem->question = $items->$strquestion;
            $tableitem->stylesid = $styleids[$items->$strstylesid];
            $itemid = $DB->insert_record("lstest_items", $tableitem);
            for ($j = 1; $j <= $test->answersnum; $j++) {
                $strnocheckedscore = "nocheckedscore" . $i . $j;
                $strcheckedscore = "checkedscore" . $i . $j;
                $tablescore->itemsid = $itemid;
                $tablescore->answersid = $answerids[$j];
                $tablescore->nocheckedscore = $items->$strnocheckedscore;
                $tablescore->checkedscore = $items->$strcheckedscore;
                $DB->insert_record("lstest_scores", $tablescore);
            }
        }
        breaK;
    }
    default:
        error();
        break;
}

redirect("$CFG->wwwroot/mod/lstest/editor/settings.php?course=$courseid");
?>
