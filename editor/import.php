<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// TODO: Use http://www.php.net/manual/en/book.simplexml.php or another XML tool to parse XML file

require_once('../../../config.php');
require_once('../locallib.php');
require($CFG->libdir . '/filelib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
lstest_editor_check_access($courseid);

require_sesskey();

$filename = $_FILES['userfile']['tmp_name'];

lstest_import_test($filename, $courseid);

redirect("$CFG->wwwroot/mod/lstest/editor/settings.php?course=$courseid", get_string('changessaved'), 1);
?>