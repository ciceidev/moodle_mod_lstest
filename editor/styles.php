<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $PAGE, $OUTPUT, $USER, $CFG;

require_once('../../../config.php');
require_once('../locallib.php');

$courseid = optional_param('course', SITEID, PARAM_INT);
require_login($courseid);
lstest_editor_check_access($courseid);
require_sesskey();

$test = lstest_get_test_submitted();

$styles = lstest_get_styles($test->stylesnum, $test->id);

if (!$test->name || !$test->lang) {
    error(get_string("filloutallfields"), $_SERVER["HTTP_REFERER"]);
}

lstest_editor_page_config($courseid);
$PAGE->set_url('/mod/lstest/editor/styles.php');

echo $OUTPUT->header();

$pageheading = get_string('addingstyles', 'lstest');
echo $OUTPUT->heading_with_help($pageheading, 'addingstyles', 'lstest');
echo $OUTPUT->box_start();

?>

<script>
    function checkform() {
        var error=false;

        <?php
        for ($i=1; $i<=$test->stylesnum; $i++) {
            $strstyle = "document.form.style".$i.".value";
            echo "  if (!".$strstyle.") error=true;\n";
        }
        ?>

        if (error) {
            alert("<?php print_string("fillallfields", "lstest") ?>");
        } else {
            document.form.submit();
        }
    }
</script>

<FORM name="form" method="post" action="<?php echo "$CFG->wwwroot/mod/lstest/editor/levels.php"; ?>">
<CENTER>
<TABLE cellpadding=5>

<?php
for ($i=1; $i<=$test->stylesnum; $i++) {
    $strstyle = "style".$i;
?>

    <TR valign=top>
        <TD align=right><P><B><?php  print_string('stylename', "lstest", $i) ?>:</B></P></TD>
        <TD>
            <INPUT type="text" name="<?php p($strstyle) ?>" size=30 value="<?php  p($styles->$strstyle) ?>">
        </TD>
    </TR>

<?php
}
?>

</TABLE>
<br>

<input type=button value=<?php print_string("continue") ?> onClick=checkform();>

<?php lstest_submit_test($test); ?>

<input type="hidden" name="sesskey" value="<?php p("$USER->sesskey") ?>">

</CENTER>
</FORM>

<?php
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>
