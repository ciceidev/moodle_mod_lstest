<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('lib.php');

/******************************
 * Views
 ******************************/

/**
 * Menu to select view test options and a heading for the option selected
 * @global type $CFG
 * @param type $courseid
 * @param type $userid
 * @param type $moduleid
 * @param type $selected
 */
function lstest_print_result_menu($courseid, $moduleid, $selected = 'view') {
    global $CFG, $OUTPUT;

    $rows = array();

    $rows[] = new tabobject('view', "$CFG->wwwroot/mod/lstest/view.php?id=$moduleid", get_string('viewtest', 'lstest'));

    $context = get_context_instance(CONTEXT_COURSE, $courseid);
    if (has_capability('mod/lstest:viewstatistics', $context)) {
        $rows[] = new tabobject('userstatistic', "$CFG->wwwroot/mod/lstest/userstatistic.php?id=$moduleid", get_string('seestudents', 'lstest'));
        $rows[] = new tabobject('stylestatistic', "$CFG->wwwroot/mod/lstest/stylestatistic.php?id=$moduleid", get_string('seestylestatistic', 'lstest'));
        $rows[] = new tabobject('itemstatistic', "$CFG->wwwroot/mod/lstest/itemstatistic.php?id=$moduleid", get_string('seeitemstatistic', 'lstest'));
        $rows[] = new tabobject('teststatistic', "$CFG->wwwroot/mod/lstest/teststatistic.php?id=$moduleid", get_string('seeteststatistic', 'lstest'));
    }

    print_tabs(array($rows), $selected);

    switch($selected) {
        case 'userstatistic':
            echo $OUTPUT->heading_with_help(get_string('seestudents', 'lstest'), 'seestudents', 'lstest');
            break;
        case 'stylestatistic':
            echo $OUTPUT->heading_with_help(get_string('seestylestatistic', 'lstest'), 'seestylestatistic', 'lstest');
            break;
        case 'itemstatistic':
            echo $OUTPUT->heading_with_help(get_string('seeitemstatistic', 'lstest'), 'seeitemstatistic', 'lstest');
            break;
        case 'teststatistic':
            echo $OUTPUT->heading_with_help(get_string('seeteststatistic', 'lstest'), 'seeteststatistic', 'lstest');
            break;
        case 'view':
        default:
            echo $OUTPUT->heading(get_string('viewtest', 'lstest'), 1);
            break;
    }
}

/**
 * Print table of scores and means obtained by the student
 * @param type $testid
 * @param type $studentscores
 * @param type $activitymean
 * @param type $coursescoresmean
 * @param type $totalscoresmean
 */
function lstest_print_result_table($testid, $studentscores, $activitymean, $coursescoresmean, $totalscoresmean) {
    global $DB;

    $table = new stdClass();
    $table->size = array('1', '1', '1', '1', '1', '1');
    $table->align = array('center', 'center', 'center', 'center', 'center', 'center');
    $table->head = array(
        get_string('style', 'lstest'),
        get_string('score', 'lstest'),
        get_string('pertenency', 'lstest'),
        get_string('activitymean', 'lstest'),
        get_string('coursemean', 'lstest'),
        get_string('totalmean', 'lstest')
    );
    $table->data = array();
    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    foreach ($styles as $style) {
        $score = $studentscores[$style->id];
        $select = "stylesid = \"$style->id\" AND infthreshold <= \"$score\" AND supthreshold >= \"$score\"";
        $thresholds = $DB->get_records_select('lstest_thresholds', $select, null, 'id asc', '*', '0', '1');
        $threshold = current($thresholds);
        $level = $DB->get_record('lstest_levels', array('id' => $threshold->levelsid));
        array_push($table->data, array($style->name, $score, $level->name, $activitymean[$style->id], $coursescoresmean[$style->id], $totalscoresmean[$style->id]));
    }
    lstest_print_table($table);
    echo '<BR>';
}

/**
 * Print answers of a student in a test
 * @param type $lstestid
 * @param type $testid
 * @param type $id
 * @param type $userid
 * @param boolean $actualstudent
 * @param boolean $coursemean
 * @param type $activitymean
 * @param type $totalmean
 * @param type $zoom
 * @param type $writemean
 * @param type $orderby
 */
function lstest_print_answer_table($lstestid, $testid, $id, $userid, $actualstudent, $coursemean, $activitymean, $totalmean, $zoom, $writemean, $orderby = "items") {
    global $DB;

    if (!isset($actualstudent) && !isset($coursemean) && !isset($activitymean) && !isset($totalmean)) {
        $actualstudent = true;
        $coursemean = true;
    }

    $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
    $firstitem = current($items);
    $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    $tablestyles = array();
    foreach ($styles as $style) {
        $tablestyles[$style->id] = array();
    }

    $counter = 1;
    $table = new stdClass();
    $table->align = array("center", "center");
    if ($orderby == "styles") {
        $table->head = array(get_string("question", "lstest") . "<BR><A HREF=userstatistic.php?id=$id&userid=$userid&actualstudent=$actualstudent&coursemean=$coursemean&activitymean=$activitymean&totalmean=$totalmean&zoom=$zoom&writemean=$writemean&orderby=items><FONT SIZE=1>" . get_string("orderbyitems", "lstest") . "</FONT></A>", get_string("style", "lstest"));
    } else {
        $table->head = array(get_string("question", "lstest") . "<BR><A HREF=userstatistic.php?id=$id&userid=$userid&actualstudent=$actualstudent&coursemean=$coursemean&activitymean=$activitymean&totalmean=$totalmean&zoom=$zoom&writemean=$writemean&orderby=styles><FONT SIZE=1>" . get_string("orderbystyles", "lstest") . "</FONT></A>", get_string("style", "lstest"));
    }
    $table->size = array("10", "10");

    foreach ($answers as $answer) {
        array_push($table->head, $answer->name);
        array_push($table->size, "10");
        array_push($table->align, "center");
    }

    $studentscores = lstest_student_item_answers($lstestid, $testid, $userid);

    if ($orderby == "styles") {
        foreach ($items as $item) {
            $tablestyles[$item->stylesid][$item->id] = array();
            foreach ($answers as $answer) {
                array_push($tablestyles[$item->stylesid][$item->id], $studentscores[$item->id][$answer->id]);
            }
        }
        foreach ($styles as $style) {
            foreach ($tablestyles[$style->id] as $key => $answersss) {
                $counter = $key - $firstitem->id + 1;
                $table->data[$counter] = array("<A HREF=\"itemstatistic.php?id=$id&item=$key\">$counter.- " . $items[$key]->question . "</A>");
                array_push($table->data[$counter], $style->name);
                foreach ($answersss as $answer) {
                    if ($answer == true) {
                        array_push($table->data[$counter], get_string("yes"));
                    } else {
                        array_push($table->data[$counter], get_string("no"));
                    }
                }
            }
        }
    } else {
        foreach ($items as $item) {
            $table->data[$item->id] = array("<A HREF=\"itemstatistic.php?id=$id&item=$item->id\">$counter.- $item->question</A>");
            array_push($table->data[$item->id], $styles[$item->stylesid]->name);
            $counter++;
            foreach ($answers as $answer) {
                if ($studentscores[$item->id][$answer->id] == true) {
                    array_push($table->data[$item->id], get_string("yes"));
                } else {
                    array_push($table->data[$item->id], get_string("no"));
                }
            }
        }
    }

    lstest_print_table($table);
}

/**
 * Print table of style statistics
 * @param type $lstestid
 * @param type $testid
 * @param type $courseid
 */
function lstest_predominance_tables($lstestid, $testid, $courseid) {
    global $DB, $OUTPUT;

    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    foreach ($styles as $style) {
        $usernumbers[$style->id] = 0;
    }
    $usernumbers['total'] = 0;
    $userids = lstest_course_students($testid, $courseid);

    $table = new stdClass();
    $table->align = array('center');

    foreach ($styles as $style) {
        $table->data = array();
        echo $OUTPUT->heading(get_string('forstylepredominance', 'lstest', $style->name));
        foreach ($userids as $userid) {
            $userstyles = lstest_user_predominance($lstestid, $testid, $userid);
            if (in_array($style->id, $userstyles)) {
                $user = $DB->get_record('user', array('id' => $userid));
                array_push($table->data, array($user->firstname . ' ' . $user->lastname));
                $usernumbers[$style->id]++;
                $usernumbers['total']++;
            }
        }
        lstest_print_table($table);
        echo '<BR>';
    }

    echo '<BR>';

    echo $OUTPUT->heading_with_help(get_string('predominance', 'lstest'), 'predominance', 'lstest');

    $context = get_context_instance(CONTEXT_COURSE, $courseid);
    $users = get_enrolled_users($context);
    if ($users) {
        $studentsnumber = count($users);
    }
    else {
        $studentsnumber = 0;
    }
    $studentswithtestnumber = count($userids);
    $table->align = array('center', 'center');
    $table->head = array(get_string('studentsinthecourse', 'lstest'), get_string('havemadethetest', 'lstest'));
    $tabledata = array($studentsnumber, $studentswithtestnumber . ' (' . ($studentsnumber != 0 ? round(count($userids) / $studentsnumber * 100) : 0) . '%)');

    foreach ($styles as $style) {
        array_push($tabledata, $usernumbers[$style->id] . ' (' . ($studentswithtestnumber != 0 ? round($usernumbers[$style->id] / $studentswithtestnumber * 100) : 0) . '%)');
        array_push($table->head, $style->name);
        array_push($table->align, 'center');
    }
    $table->data = array($tabledata);

    lstest_print_table($table);
}

/**
 *
 * @param type $lstestid
 * @param type $testid
 * @param type $courseid
 * @param type $moduleid
 * @param type $itemid
 * @param type $orderby
 */
function lstest_print_items_table($lstestid, $testid, $courseid, $moduleid, $itemid = "", $orderby = "") {
    global $DB;

    $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
    $firstitem = current($items);
    $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
    $answersnum = count($answers);
    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    $tablestyles = array();
    foreach ($styles as $style) {
        $tablestyles[$style->id] = array();
    }

    $table = new stdClass();
    $table->align = array("center", "center");
    $table->data[0] = array("", "");
    $table->headcolspan = array("1", "1", "$answersnum", "$answersnum", "$answersnum");
    if ($orderby == "styles") {
        $table->head = array(get_string("question", "lstest") . "<BR><A HREF=itemstatistic.php?id=$moduleid&item=$itemid&orderby=items><FONT SIZE=1>" . get_string("orderbyitems", "lstest") . "</FONT></A>", get_string("style", "lstest"));
    } else {
        $table->head = array(get_string("question", "lstest") . "<BR><A HREF=itemstatistic.php?id=$moduleid&item=$itemid&orderby=styles><FONT SIZE=1>" . get_string("orderbystyles", "lstest") . "</FONT></A>", get_string("style", "lstest"));
    }

    for ($i = 0; $i < 3; $i++) {
        foreach ($answers as $answer) {
            array_push($table->align, "center");
            array_push($table->data[0], "<FONT size=1>$answer->name</FONT>");
        }
    }
    array_push($table->head, get_string("intheactivity", "lstest"));
    array_push($table->head, get_string("inthecourse", "lstest"));
    array_push($table->head, get_string("inmoodle", "lstest"));

    $resultanswers[0] = lstest_activity_item_answers($lstestid, $testid);
    $resultanswers[1] = lstest_course_item_answers($testid, $courseid);
    $resultanswers[2] = lstest_all_users_item_answers($testid);


    if ($orderby == "styles") {
        foreach ($items as $item) {
            $tablestyles[$item->stylesid][$item->id] = array();
            for ($i = 0; $i < 3; $i++) {
                foreach ($answers as $answer) {
                    array_push($tablestyles[$item->stylesid][$item->id], $resultanswers[$i][$item->id][$answer->id]);
                }
            }
        }
        foreach ($styles as $style) {
            foreach ($tablestyles[$style->id] as $key => $answers) {
                $counter = $key - $firstitem->id + 1;
                $table->data[$counter] = array("<A HREF=itemstatistic.php?id=$moduleid&item=$key&orderby=$orderby>$counter.- " . $items[$key]->question . "</A>");
                array_push($table->data[$counter], $style->name);
                foreach ($answers as $answer) {
                    array_push($table->data[$counter], $answer);
                }
            }
        }
    } else {
        $counter = 1;
        foreach ($items as $item) {
            $table->data[$item->id] = array("<A HREF=itemstatistic.php?id=$moduleid&item=$item->id&orderby=$orderby>$counter.- $item->question</A>");
            array_push($table->data[$item->id], $styles[$item->stylesid]->name);
            $counter++;
            for ($i = 0; $i < 3; $i++) {
                foreach ($answers as $answer) {
                    array_push($table->data[$item->id], $resultanswers[$i][$item->id][$answer->id]);
                }
            }
        }
    }
    lstest_print_table($table);
}

/**
 *
 * @param type $lstestid
 * @param type $testid
 * @param type $itemid
 * @param type $courseid
 * @param type $moduleid
 */
function lstest_print_item_table($lstestid, $testid, $itemid, $courseid, $moduleid) {
    global $DB;

    $students = lstest_activity_students($lstestid);
    $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
    $answersnum = count($answers);

    $table->align = array("center", "center", "center");
    $table->size = array("10", "10", "10");
    $table->head = array(get_string("student", "lstest"));
    foreach ($answers as $answer) {
        array_push($table->head, $answer->name);
    }

    if ($students) {

        $counter = 1;

        foreach ($students as $studentid) {
            $studentanswers = $DB->get_records_select('lstest_user_answers', "lstestid = '$lstestid' AND itemsid = '$itemid' AND userid = '$studentid'", null, 'time desc', '*', '0', $answersnum);
            if ($studentanswers) {
                $user = $DB->get_record('user', array('id' => $studentid));
                $table->data[$counter] = array("<A HREF=\"userstatistic.php?id=$moduleid&userid=$user->id\">" . $user->firstname . " " . $user->lastname . "</A>");

                foreach ($studentanswers as $studentanswer) {
                    if ($studentanswer->checked) {
                        array_push($table->data[$counter], get_string('yes'));
                    } else {
                        array_push($table->data[$counter], get_string('no'));
                    }
                }


                $counter++;
            }
        }
    }
    lstest_print_table($table);
}

/**
 * Internal function to print a table of values
 * @param type $table
 * @return boolean
 */
function lstest_print_table($table) {
    global $OUTPUT;

    if (isset($table->align)) {
        foreach ($table->align as $key => $aa) {
            if ($aa) {
                $align[$key] = " align=\"$aa\"";
            } else {
                $align[$key] = "";
            }
        }
    }
    if (isset($table->size)) {
        foreach ($table->size as $key => $ss) {
            if ($ss) {
                $size[$key] = " width=\"$ss\"";
            } else {
                $size[$key] = "";
            }
        }
    }
    if (isset($table->headcolspan)) {
        foreach ($table->headcolspan as $key => $cc) {
            if ($cc) {
                $headcolspan[$key] = " colspan=\"$cc\"";
            } else {
                $headcolspan[$key] = "";
            }
        }
    }
    if (isset($table->wrap)) {
        foreach ($table->wrap as $key => $ww) {
            if ($ww) {
                $wrap[$key] = " nowrap=\"nowrap\" ";
            } else {
                $wrap[$key] = "";
            }
        }
    }

    if (empty($table->width)) {
        $table->width = "80%";
    }

    if (empty($table->cellpadding)) {
        $table->cellpadding = "5";
    }

    if (empty($table->cellspacing)) {
        $table->cellspacing = "1";
    }

    echo $OUTPUT->box_start();
    echo "<table width=\"100%\" border=\"0\" valign=\"top\" align=\"center\" ";
    echo " cellpadding=\"$table->cellpadding\" cellspacing=\"$table->cellspacing\" class=\"generaltable\">\n";

    $countcols = 0;

    if (!empty($table->head)) {
        $countcols = count($table->head);
        echo "<tr>";
        foreach ($table->head as $key => $heading) {

            if (!isset($size[$key])) {
                $size[$key] = "";
            }
            if (!isset($align[$key])) {
                $align[$key] = "";
            }
            if (!isset($headcolspan[$key])) {
                $headcolspan[$key] = "";
            }
            echo "<th valign=\"middle\" " . $align[$key] . $size[$key] . $headcolspan[$key] . " class=\"generaltableheader\">$heading</th>";
        }
        echo "</tr>\n";
    }

    if (!empty($table->data)) {
        $countcols = count($table->align);
        foreach ($table->data as $row) {
            echo "<tr valign=\"middle\">";
            if ($row == "hr" and $countcols) {
                echo "<td colspan=\"$countcols\"><div class=\"tabledivider\"></div></td>";
            } else {  /// it's a normal row of data
                foreach ($row as $key => $item) {
                    if (!isset($size[$key])) {
                        $size[$key] = "";
                    }
                    if (!isset($align[$key])) {
                        $align[$key] = "";
                    }
                    if (!isset($wrap[$key])) {
                        $wrap[$key] = "";
                    }
                    echo "<td " . $align[$key] . $size[$key] . $wrap[$key] . " class=\"generaltablecell\">$item</td>";
                }
            }
            echo "</tr>\n";
        }
    }
    echo "</table>\n";
    echo $OUTPUT->box_end();

    return true;
}

/******************************************
 * Data
 ******************************************/

/**
 * Return latest completed date for a user in a test instance
 * @param type $userid
 * @param type $lstestid
 * @return type
 */
function lstest_completed_date($userid, $lstestid) {
    global $DB;

    $select = "lstestid = '$lstestid' AND userid = '$userid'";
    $userscores = $DB->get_records_select('lstest_user_scores', $select, null, 'time desc', '*', '0', '1');
    $lastuserscore = current($userscores);
    return $lastuserscore->time;
}

/**
 * Returns an array of scores obtained by a student in a test instance
 * @param type $lstestid
 * @param type $userid
 * @return Array of [styleid] = score
 */
function lstest_student_scores($lstestid, $userid) {
    global $DB;

    $stylescores = array();
    // Get test instance scores
    $scores = $DB->get_records('lstest_user_scores', array('lstestid' => $lstestid, 'userid' => $userid));
    if ($scores) {
        // Return scores array indexed by style id
        foreach ($scores as $score) {
            $stylescores[$score->stylesid] = $score->score;
        }
    }
    return count($stylescores) > 0 ? $stylescores : NULL;
}

/**
 * Returns an array of scores obtained by a student in same test type instances within a course
 * @param type $testid
 * @param type $courseid
 * @param type $userid
 * @param type $sum_results If true, scores are added
 * @return Array of [lstestid][styleid] = score or [styleid] = score if are calculated as an addition
 */
function lstest_student_scores_course($testid, $courseid, $userid, $sum_results = false) {
    global $DB;

    $stylescores = array();
    // Get course test instances
    $lstests = $DB->get_records('lstest', array('testsid' => $testid, 'course' => $courseid));
    if ($lstests) {
        // For each instance, get scores obtained by the student
        foreach ($lstests as $lstest) {
            $scores = lstest_student_scores($lstest->id, $userid);
            if($scores) {
                foreach ($scores as $styleid => $score) {
                    $stylescores[$lstest->id][$styleid] = $score;
                }
            }
        }
        if($sum_results) {
            $stylescores = lstest_sum_scores_array($stylescores);
        }
    }
    return count($stylescores) > 0 ? $stylescores : NULL;
}

/**
 * Returns an array of of scores obtained by a student in all tests instances.
 * @param type $testid
 * @param type $userid
 * @param type $sum_results If true, scores are added
 * @return Array of [lstestid][styleid] = score or [styleid] = score if are calculated as an addition
 */
function lstest_student_scores_all($testid, $userid, $sum_results = false) {
    global $DB;

    $stylescores = array();
    // Get all test instances
    $lstests = $DB->get_records('lstest', array('testsid' => $testid));
    if ($lstests) {
        // Get styles
        $styles = $DB->get_records('lstest_styles', array('testsid' => $testid));
        // For each instance, get scores obtained by the student
        foreach ($lstests as $lstest) {
            // Initialize scores to 0
            foreach ($styles as $style) {
                $stylescores[$lstest->id][$style->id] = 0;
            }
            // Load student scores in a test
            $scores = lstest_student_scores($lstest->id, $userid);
            if($scores) {
                foreach ($scores as $styleid => $score) {
                    $stylescores[$lstest->id][$styleid] = $score;
                }
            }
        }
        if($sum_results) {
            $stylescores = lstest_sum_scores_array($stylescores);
        }
    }
    return count($stylescores) > 0 ? $stylescores : NULL;
}

/**
 * Sum arrays of scores generated by scores_course and scores_all functions
 * @param type $scores [lstestid][styleid] = score
 * @return Array of [styleid] = sum score
 */
function lstest_sum_scores_array($scores) {
    $return = array();
    foreach($scores as $lstestid => $styles) {
        foreach($styles as $styleid => $score) {
            if(!isset($return[$styleid])) {
                $return[$styleid] = $score;
            }
            else {
                $return[$styleid] += $score;
            }
        }
    }
    return $return;
}

/**
 * Returns an array of tests scores means for activity, course and platform
 * @param type $lstestid
 * @param type $testid
 * @param type $courseid
 * @return string
 */
function lstest_mean_scores($lstestid, $testid, $courseid) {
    global $DB;

    // Init means array
    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    $resultscores = array('all' => array(), 'course' => array(), 'category' => array());
    foreach ($styles as $style) {
        $resultscores['activity'][$style->id] = 0;
        $resultscores['course'][$style->id] = 0;
        $resultscores['all'][$style->id] = 0;
    }

    // Get students that completed this test per context
    $activitystudentsids = lstest_activity_students($lstestid);
    $coursestudentsids = lstest_course_students($testid, $courseid);
    $allstudentsids = lstest_all_students($testid);

    // Count scores for each style
    foreach ($allstudentsids as $studentid) {

        $activityscores = lstest_student_scores($lstestid, $studentid);
        $coursescores = lstest_student_scores_course($testid, $courseid, $studentid, true);
        $allscores = lstest_student_scores_all($testid, $studentid, true);

        foreach ($styles as $style) {
            $styleid = $style->id;
            // Platform score
            if($allscores) {
                $resultscores['all'][$styleid] += $allscores[$styleid];
            }
            // Course score
            if (in_array($studentid, $coursestudentsids) && $coursescores) {
                $resultscores['course'][$styleid] += $coursescores[$styleid];
            }
            // Activity score
            if (in_array($studentid, $activitystudentsids) && $activityscores) {
                $resultscores['activity'][$styleid] += $activityscores[$styleid];
            }
        }
    }

    // Count students that have completed this test
    $activitystudentsnum = count($activitystudentsids);
    $coursestudentsnum = count($coursestudentsids);
    $allstudentsnum = count($allstudentsids);

    foreach ($styles as $style) {
        // Activity mean
        if ($activitystudentsnum != 0) {
            $resultscores['activity'][$style->id] = round($resultscores['activity'][$style->id] / $activitystudentsnum, 2);
        } else {
            $resultscores['activity'][$style->id] = '';
        }
        // Course mean
        if ($coursestudentsnum != 0) {
            $resultscores['course'][$style->id] = round($resultscores['course'][$style->id] / $coursestudentsnum, 2);
        } else {
            $resultscores['course'][$style->id] = '';
        }
        // Platform mean
        if ($allstudentsnum != 0) {
            $resultscores['all'][$style->id] = round($resultscores['all'][$style->id] / $allstudentsnum, 2);
        } else {
            $resultscores['all'][$style->id] = '';
        }
    }
    return $resultscores;
}

/**
 * Return all students ids that completed a test instance
 * @param type $lstestid
 * @return type
 */
function lstest_activity_students($lstestid) {
    global $DB;

    $usersids = array();
    $scores = $DB->get_records('lstest_user_scores', array('lstestid' => $lstestid));

    if ($scores) {
        foreach ($scores as $score) {
            array_push($usersids, $score->userid);
        }
        $usersids = array_unique($usersids);
    }
    return $usersids;
}

/**
 * Returns all students ids that completed a type of test within a course
 * @param type $testid
 * @param type $courseid
 * @return type
 */
function lstest_course_students($testid, $courseid) {
    global $DB;

    $usersids = array();
    $lstests = $DB->get_records('lstest', array('testsid' => $testid, 'course' => $courseid));

    if ($lstests) {
        foreach ($lstests as $lstest) {
            $activitystudentsids = lstest_activity_students($lstest->id);
            $usersids = array_merge($usersids, $activitystudentsids);
        }
        $usersids = array_unique($usersids);
    }
    return $usersids;
}

/**
 * Returns all students ids that completed a type of test in all courses
 * @param type $testid
 * @return type
 */
function lstest_all_students($testid) {
    global $DB;

    $usersids = array();
    $lstests = $DB->get_records('lstest', array('testsid' => $testid));

    if ($lstests) {
        foreach ($lstests as $lstest) {
            $activitystudentsids = lstest_activity_students($lstest->id);
            $usersids = array_merge($usersids, $activitystudentsids);
        }
        $usersids = array_unique($usersids);
    }
    return $usersids;
}

/**
 * Returns an array of answers of a student in a test instance
 * @param type $lstestid
 * @param type $testid
 * @param type $userid
 * @return boolean|null
 */
function lstest_student_item_answers($lstestid, $testid, $userid) {
    global $DB;

    $itemanswers = array();
    $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');

    foreach ($items as $item) {
        // Some functions use $lstestid=0 to get all possible responses
        $select = $lstestid ? "lstestid = '$lstestid' AND " : '';
        $select .= "itemsid = '$item->id' AND userid = '$userid'";
        $useranswers = $DB->get_records_select('lstest_user_answers', $select);
        if ($useranswers) {
            foreach ($useranswers as $useranswer) {
                if ($useranswer->checked) {
                    $itemanswers[$item->id][$useranswer->answersid] = true;
                } else {
                    $itemanswers[$item->id][$useranswer->answersid] = false;
                }
            }
        }
    }
    return count($itemanswers) > 0 ? $itemanswers : NULL;
}

/**
 * Returns an array of predominant styles for a user and test instance
 * @param type $lstestid
 * @param type $testid
 * @param type $userid
 * @return boolean
 */
function lstest_user_predominance($lstestid, $testid, $userid) {
    global $DB;

    $userlevels = array();
    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');

    foreach ($styles as $style) {
        $select = "lstestid = '$lstestid' AND stylesid = '$style->id' AND userid = '$userid'";
        $userscores = $DB->get_records_select('lstest_user_scores', $select, null, 'time desc', '*', '0', '1');
        if ($userscores) {
            $userscore = current($userscores);
            $userlevels[$style->id] = $userscore->levelsid;
        }
    }

    if (!empty($userlevels)) {
        $result = array();
        foreach ($styles as $style) {
            $ispredominant = true;
            foreach ($userlevels as $userlevel) {
                $ispredominant = ($userlevels[$style->id] >= $userlevel);
                if (!$ispredominant) {
                    break;
                }
            }
            if ($ispredominant) {
                array_push($result, $style->id);
            }
        }
    } else {
        $result = false;
    }
    return $result;
}

/**
 * Returns count of answers for each item (question) in a test instance
 * @param type $lstestid
 * @param type $testid
 * @return array
 */
function lstest_activity_item_answers($lstestid, $testid) {
    global $DB;

    $resultanswers = array();

    $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
    $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
    foreach ($items as $item) {
        foreach ($answers as $answer) {
            $select = "lstestid = '$lstestid' AND itemsid = '$item->id' AND answersid = '$answer->id' AND checked = '1'";
            $user_answers = $DB->get_records_select('lstest_user_answers', $select);
            $resultanswers[$item->id][$answer->id] = $user_answers ? count($user_answers) : 0;
        }
    }
    return $resultanswers;
}

/**
 * Returns count of answers for each item (question) in all test instances within a course
 * @param type $testid
 * @param type $courseid
 */
function lstest_course_item_answers($testid, $courseid) {
    global $DB;

    $resultanswers = array();

    $select = "testsid = '$testid' AND course = '$courseid'";
    $lstests = $DB->get_records_select('lstest', $select);
    if ($lstests) {
        foreach ($lstests as $lstest) {
            $activity_answers = lstest_activity_item_answers($lstest->id, $testid);
            if ($activity_answers) {
                foreach ($activity_answers as $itemid => $answer) {
                    foreach ($answer as $answerid => $count) {
                        if(!isset($resultanswers[$itemid][$answerid])) {
                            $resultanswers[$itemid][$answerid] = 0;
                        }
                        $resultanswers[$itemid][$answerid] += $count;
                    }
                }
            }
        }
    }
    return $resultanswers;
}

/**
 * Returns count of answers for each item (question) in all test instances
 * @param type $testid
 * @return array
 */
function lstest_all_users_item_answers($testid) {
    global $DB;

    $resultanswers = array();

    $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
    $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
    foreach ($items as $item) {
        foreach ($answers as $answer) {
            $select = "itemsid = '$item->id' AND answersid = '$answer->id' AND checked = '1'";
            $user_answers = $DB->get_records_select('lstest_user_answers', $select);
            $resultanswers[$item->id][$answer->id] = $user_answers ? count($user_answers) : 0;
        }
    }
    return $resultanswers;
}

/**
 * Editor util functions
 */

function lstest_editor_check_access($courseid = 0) {
    $context = get_context_instance(CONTEXT_COURSE, $courseid);
    require_capability('mod/lstest:editor', $context, null, true, 'youcannotchange', 'lstest');
}

/**
 * Check if current user can edit a test
 * @param type $testid
 * @return boolean
 */
function lstest_can_edit_test($testid) {
    global $DB;

    require_login();

    // Admins can edit any test
    if(is_siteadmin()) {
        return true;
    }
    else {
        // Teachers can edit only tests of their courses
        $test = $DB->get_record('lstest_tests', array('id' => $testid));
        if($test) {
            $context = get_context_instance(CONTEXT_COURSE, $test->courseid);
            if (has_capability('mod/lstest:editor', $context)) {
                return true;
            }
        }
    }
    return false;
}

/**
 * Sets global $PAGE parameters for editor pages
 * @param type $courseid
 */
function lstest_editor_page_config($courseid) {
    global $PAGE;

    $strmodulename = get_string('modulename', 'lstest');
    $streditingtest = get_string('editingstyletest', 'lstest');

    $PAGE->set_title("$strmodulename - $streditingtest");
    $PAGE->set_heading($streditingtest);
    // Configure page layout inside a course
    if ($courseid != SITEID) {
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        $layout = 'incourse';
    }
    // Configure page layout inside admin
    else {
        $context = get_context_instance(CONTEXT_SYSTEM);
        $layout = 'admin';
        $PAGE->navbar->add(get_string('administrationsite'));
        $PAGE->navbar->add(get_string('plugins', 'admin'));
        $PAGE->navbar->add(get_string('activitymodules'));
    }
    $PAGE->set_context($context);
    $PAGE->set_pagelayout($layout);
    $PAGE->navbar->add($strmodulename);
    $PAGE->navbar->add($streditingtest);
}

/**
 * Import a test from a file
 * @param type $filename
 */
function lstest_import_test($filename, $courseid=0) {
    global $DB;

    // Load xml file
    $xml=simplexml_load_file($filename);
    if (!$xml) {
        print_error("nofile", "lstest");
    }

    // Check xml
    if (empty($xml->NAME)) {
        print_error("withoutname", "lstest");
    }
    if (empty($xml->LANG)) {
        print_error("withoutlang", "lstest");
    }
    if (empty($xml->STYLES)) {
        print_error("withoutstyles", "lstest");
    }
    if (empty($xml->LEVELS)) {
        print_error("withoutlevels", "lstest");
    }
    if (empty($xml->THRESHOLDS)) {
        print_error("withoutthresholds", "lstest");
    }
    if (empty($xml->ANSWERS)) {
        print_error("withoutanswers", "lstest");
    }
    if (empty($xml->ITEMS)) {
        print_error("withoutitems", "lstest");
    }
    if (empty($xml->SCORES)) {
        print_error("withoutscores", "lstest");
    }

    // Test object
    $test = new stdClass();
    $test->name = (string) $xml->NAME;
    $test->lang = (string) $xml->LANG;
    $test->available = (string) $xml->AVAILABLE;
    $test->redoallowed = (string) $xml->REDOALLOWED;
    $test->multipleanswer = (string) $xml->MULTIPLEANSWER;
    $test->notansweredquestion = (string) $xml->NOTANSWEREDQUESTION;
    $test->courseid = $courseid;
    $test->id = $DB->insert_record('lstest_tests', $test);

    // STyles objects
    $styles = array();
    foreach ($xml->STYLES->STYLE as $style_name) {
        $style = new stdClass();
        $style->name = (string) $style_name;
        $style->testsid = $test->id;
        $style->id = $DB->insert_record('lstest_styles', $style);
        $styles[] = $style;
    }

    // Levels objects
    $levels = array();
    foreach ($xml->LEVELS->LEVEL as $level_name) {
        $level = new stdClass();
        $level->name = (string) $level_name;
        $level->testsid = $test->id;
        $level->id = $DB->insert_record('lstest_levels', $level);
        $levels[] = $level;
    }

    // Answers objects
    $answers = array();
    foreach ($xml->ANSWERS->ANSWER as $answer_name) {
        $answer = new stdClass();
        $answer->name =  (string) $answer_name;
        $answer->testsid = $test->id;
        $answer->id = $DB->insert_record('lstest_answers', $answer);
        $answers[] = $answer;
    }

    // Items objects
    $items = array();
    foreach ($xml->ITEMS->ITEM as $item_element) {
        $item = new stdClass();
        $item->stylesid = $styles[$item_element->ITEMSTYLE-1]->id;
        $item->question = (string) $item_element->QUESTION;
        $item->testsid = $test->id;
        $item->id = $DB->insert_record('lstest_items', $item);
        $items[] = $item;
    }

    // Scores objects
    //$scores = array();
    foreach ($xml->SCORES->SCORE as $score_element) {
        $score = new stdClass();
        $score->itemsid = $items[$score_element->SCOREITEM-1]->id;
        $score->answersid = $answers[$score_element->SCOREANSWER-1]->id;
        $score->nocheckedscore = (string) $score_element->NOCHECKEDSCORE;
        $score->checkedscore = (string) $score_element->CHECKEDSCORE;
        $score->id = $DB->insert_record('lstest_scores', $score);
        //$scores[] = $score;
    }

    // Thresholds objects
    //$thresholds = array();
    foreach ($xml->THRESHOLDS->THRESHOLD as $threshold_element) {
        $threshold = new stdClass();
        $threshold->stylesid = $styles[$threshold_element->THRESHOLDSTYLE-1]->id;
        $threshold->levelsid = $levels[$threshold_element->THRESHOLDLEVEL-1]->id;
        $threshold->infthreshold = (string) $threshold_element->INFTHRESHOLD;
        $threshold->supthreshold = (string) $threshold_element->SUPTHRESHOLD;
        $threshold->id = $DB->insert_record('lstest_thresholds', $threshold);
        //$thresholds[] = $threshold;
    }
}

/**
 *  Return general information related to a existing test or default values to
 *  create a new one
 * @param int $testid
 * @return array
 */
function lstest_get_test($testid = 0) {
    global $CFG, $DB;

    if ($testid) {
        $testrecord = $DB->get_record('lstest_tests', array('id' => $testid));
        $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
        $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
        $levels = $DB->get_records('lstest_levels', array('testsid' => $testid), 'id asc');
        $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');

        $test->id = $testid;
        $test->name = $testrecord->name;
        $test->lang = $testrecord->lang;
        $test->stylesnum = count($styles);
        $test->levelsnum = count($levels);
        $test->answersnum = count($answers);
        $test->itemsnum = count($items);
        $test->available = $testrecord->available;
        $test->redoallowed = $testrecord->redoallowed;
        $test->multipleanswer = $testrecord->multipleanswer;
        $test->notansweredquestion = $testrecord->notansweredquestion;
    }
    else {
        $test->id = '';
        $test->name = '';
        $test->lang = $CFG->lang;
        $test->stylesnum = '4';
        $test->levelsnum = '5';
        $test->itemsnum = '80';
        $test->answersnum = '2';
        $test->available = '1';
        $test->redoallowed = '0';
        $test->multipleanswer = '0';
        $test->notansweredquestion = '0';
    }
    return $test;
}

function lstest_get_styles($stylesnum, $testid) {
    global $DB;

    if (!empty($testid)) {
        $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($styles as $onestyle) {
            $stylenames[$i++] = $onestyle->name;
        }
        for ($i = 1; $i <= $stylesnum; $i++) {
            $strstyle = "style" . $i;
            $style->$strstyle = $stylenames[$i];
        }
    } else {
        for ($i = 1; $i <= $stylesnum; $i++) {
            $strstyle = "style" . $i;
            $style->$strstyle = "";
        }
    }
    return $style;
}

function lstest_get_levels($levelsnum, $testid) {
    global $DB;

    if (!empty($testid)) {
        $levels = $DB->get_records('lstest_levels', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($levels as $onelevel) {
            $levelnames[$i++] = $onelevel->name;
        }
        for ($i = 1; $i <= $levelsnum; $i++) {
            $strlevel = "level" . $i;
            $level->$strlevel = $levelnames[$i];
        }
    } else {
        for ($i = 1; $i <= $levelsnum; $i++) {
            $strlevel = "level" . $i;
            $level->$strlevel = "";
        }
    }
    return $level;
}

function lstest_get_thresholds($stylesnum, $levelsnum, $testid) {
    global $DB;

    if (!empty($testid)) {
        $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
        $levels = $DB->get_records('lstest_levels', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($styles as $onestyle) {
            $styleids[$i++] = $onestyle->id;
        }
        $i = 1;
        foreach ($levels as $onelevel) {
            $levelids[$i++] = $onelevel->id;
        }
        for ($i = 1; $i <= $stylesnum; $i++) {
            for ($j = 1; $j <= $levelsnum; $j++) {
                $strinfthreshold = "infthreshold" . $i . $j;
                $strsupthreshold = "supthreshold" . $i . $j;
                $threshold = $DB->get_record('lstest_thresholds', array('stylesid' => $styleids[$i], 'levelsid' => $levelids[$j]));
                $thresholds->$strinfthreshold = $threshold->infthreshold;
                $thresholds->$strsupthreshold = $threshold->supthreshold;
            }
        }
    } else {
        for ($i = 1; $i <= $stylesnum; $i++) {
            for ($j = 1; $j <= $levelsnum; $j++) {
                $strinfthreshold = "infthreshold" . $i . $j;
                $strsupthreshold = "supthreshold" . $i . $j;
                $thresholds->$strinfthreshold = 1;
                $thresholds->$strsupthreshold = 1;
            }
        }
    }
    return $thresholds;
}

function lstest_get_answers($answersnum, $testid) {
    global $DB;

    if (!empty($testid)) {
        $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($answers as $oneanswer) {
            $answernames[$i++] = $oneanswer->name;
        }
        for ($i = 1; $i <= $answersnum; $i++) {
            $stranswer = "answer" . $i;
            $answer->$stranswer = $answernames[$i];
        }
    } else {
        for ($i = 1; $i <= $answersnum; $i++) {
            $stranswer = "answer" . $i;
            $answer->$stranswer = "";
        }
    }
    return $answer;
}

function lstest_get_items($itemsnum, $answersnum, $testid) {
    global $DB;

    if (!empty($testid)) {
        $items = $DB->get_records('lstest_items', array('testsid' => $testid), 'id asc');
        $answers = $DB->get_records('lstest_answers', array('testsid' => $testid), 'id asc');
        $i = 1;
        foreach ($items as $oneitem) {
            $itemids[$i] = $oneitem->id;
            $itemquestions[$i] = $oneitem->question;
            $itemstylesids[$i++] = $oneitem->stylesid;
        }
        $i = 1;
        foreach ($answers as $oneanswer) {
            $answerids[$i++] = $oneanswer->id;
        }
        for ($i = 1; $i <= $itemsnum; $i++) {
            $strquestion = "question" . $i;
            $strstylesid = "stylesid" . $i;
            $item->$strquestion = $itemquestions[$i];
            $item->$strstylesid = $itemstylesids[$i];
            for ($j = 1; $j <= $answersnum; $j++) {
                $strnocheckedscore = "nocheckedscore" . $i . $j;
                $strcheckedscore = "checkedscore" . $i . $j;
                $score = $DB->get_record('lstest_scores', array('itemsid' => $itemids[$i], 'answersid' => $answerids[$j]));
                $item->$strnocheckedscore = $score->nocheckedscore;
                $item->$strcheckedscore = $score->checkedscore;
            }
        }
    } else {
        for ($i = 1; $i <= $itemsnum; $i++) {
            $strquestion = "question" . $i;
            $strstylesid = "stylesid" . $i;
            $item->$strquestion = "";
            $item->$strstylesid = "1";
            for ($j = 1; $j <= $answersnum; $j++) {
                $strnocheckedscore = "nocheckedscore" . $i . $j;
                $strcheckedscore = "checkedscore" . $i . $j;
                $item->$strnocheckedscore = 0;
                $item->$strcheckedscore = 0;
            }
        }
    }
    return $item;
}

/**
 * Editor forms util functions
 */

function lstest_submit_items($itemsnum, $answersnum, $items) {
    for ($i = 1; $i <= $itemsnum; $i++) {
        $strquestion = "question" . $i;
        $strstylesid = "stylesid" . $i;
        echo "<input type='hidden' name='$strquestion' value='" . $items->$strquestion . "'>";
        echo "<input type='hidden' name='$strstylesid' value='" . $items->$strstylesid . "'>";
        for ($j = 1; $j <= $answersnum; $j++) {
            $strnocheckedscore = "nocheckedscore" . $i . $j;
            $strcheckedscore = "checkedscore" . $i . $j;
            echo "<input type='hidden' name='$strnocheckedscore' value='" . $items->$strnocheckedscore . "'>";
            echo "<input type='hidden' name='$strcheckedscore' value='" . $items->$strcheckedscore . "'>";
        }
    }
}

function lstest_get_items_submitted($itemsnum, $answersnum) {
    if (!empty($_POST)) {
        $data = (object) $_POST;
        for ($i = 1; $i <= $itemsnum; $i++) {
            $strquestion = "question" . $i;
            $strstylesid = "stylesid" . $i;
            $items->$strquestion = $data->$strquestion;
            $items->$strstylesid = $data->$strstylesid;
            for ($j = 1; $j <= $answersnum; $j++) {
                $strnocheckedscore = "nocheckedscore" . $i . $j;
                $strcheckedscore = "checkedscore" . $i . $j;
                $items->$strnocheckedscore = $data->$strnocheckedscore;
                $items->$strcheckedscore = $data->$strcheckedscore;
            }
        }
        return $items;
    } else {
        return false;
    }
}

function lstest_submit_answers($answersnum, $answers) {
    for ($i = 1; $i <= $answersnum; $i++) {
        $stranswer = "answer" . $i;
        echo "<input type='hidden' name='$stranswer' value='" . $answers->$stranswer . "'>";
    }
}

function lstest_get_answers_submitted($answersnum) {
    if (!empty($_POST)) {
        $data = (object) $_POST;
        for ($i = 1; $i <= $answersnum; $i++) {
            $stranswer = "answer" . $i;
            $answers->$stranswer = $data->$stranswer;
        }
        return $answers;
    } else {
        return false;
    }
}

function lstest_submit_thresholds($stylesnum, $levelsnum, $thresholds) {
    for ($i = 1; $i <= $stylesnum; $i++) {
        for ($j = 1; $j <= $levelsnum; $j++) {
            $strinfthreshold = "infthreshold" . $i . $j;
            $strsupthreshold = "supthreshold" . $i . $j;
            echo "<input type='hidden' name='$strinfthreshold' value='" . $thresholds->$strinfthreshold . "'>";
            echo "<input type='hidden' name='$strsupthreshold' value='" . $thresholds->$strsupthreshold . "'>";
        }
    }
}

function lstest_get_thresholds_submitted($stylesnum, $levelsnum) {
    if (!empty($_POST)) {
        $data = (object) $_POST;
        for ($i = 1; $i <= $stylesnum; $i++) {
            for ($j = 1; $j <= $levelsnum; $j++) {
                $strinfthreshold = "infthreshold" . $i . $j;
                $strsupthreshold = "supthreshold" . $i . $j;
                $thresholds->$strinfthreshold = $data->$strinfthreshold;
                $thresholds->$strsupthreshold = $data->$strsupthreshold;
            }
        }
        return $thresholds;
    } else {
        return false;
    }
}

function lstest_submit_levels($levelsnum, $levels) {
    for ($i = 1; $i <= $levelsnum; $i++) {
        $strlevel = "level" . $i;
        echo "<input type='hidden' name='$strlevel' value='" . $levels->$strlevel . "'>";
    }
}

function lstest_get_levels_submitted($levelsnum) {
    if (!empty($_POST)) {
        $data = (object) $_POST;
        for ($i = 1; $i <= $levelsnum; $i++) {
            $strlevel = "level" . $i;
            $levels->$strlevel = $data->$strlevel;
        }
        return $levels;
    } else {
        return false;
    }
}

function lstest_submit_styles($stylesnum, $styles) {
    for ($i = 1; $i <= $stylesnum; $i++) {
        $strstyle = "style" . $i;
        echo "<input type='hidden' name='$strstyle' value='" . $styles->$strstyle . "'>";
    }
}

function lstest_get_styles_submitted($stylesnum) {
    if (!empty($_POST)) {
        $data = (object) $_POST;
        for ($i = 1; $i <= $stylesnum; $i++) {
            $strstyle = "style" . $i;
            $styles->$strstyle = $data->$strstyle;
        }
        return $styles;
    } else {
        return false;
    }
}

function lstest_get_test_submitted() {
    if (!empty($_POST)) {
        $data = (object) $_POST;

        $test->id = $data->id;
        $test->name = $data->name;
        $test->lang = $data->lang;
        $test->stylesnum = $data->stylesnum;
        $test->levelsnum = $data->levelsnum;
        $test->answersnum = $data->answersnum;
        $test->itemsnum = $data->itemsnum;
        $test->available = $data->available;
        $test->redoallowed = $data->redoallowed;
        $test->multipleanswer = $data->multipleanswer;
        $test->notansweredquestion = $data->notansweredquestion;
        $test->courseid = $data->course;
        return $test;
    } else {
        return false;
    }
}

function lstest_submit_test($test) {
    echo "<input type='hidden' name='id' value='$test->id'>";
    echo "<input type='hidden' name='name' value='$test->name'>";
    echo "<input type='hidden' name='lang' value='$test->lang'>";
    echo "<input type='hidden' name='stylesnum' value='$test->stylesnum'>";
    echo "<input type='hidden' name='levelsnum' value='$test->levelsnum'>";
    echo "<input type='hidden' name='answersnum' value='$test->answersnum'>";
    echo "<input type='hidden' name='itemsnum' value='$test->itemsnum'>";
    echo "<input type='hidden' name='available' value='$test->available'>";
    echo "<input type='hidden' name='redoallowed' value='$test->redoallowed'>";
    echo "<input type='hidden' name='multipleanswer' value='$test->multipleanswer'>";
    echo "<input type='hidden' name='notansweredquestion' value='$test->notansweredquestion'>";
    echo "<input type='hidden' name='course' value='$test->courseid'>";
}

/******************************
 * Graph functions
 ******************************/

/**
 * Print a img link to generate a graph
 * @global type $CFG
 * @param type $moduleid
 * @param type $userid
 * @param type $actualstudent
 * @param type $coursemean
 * @param type $activitymean
 * @param type $totalmean
 * @param type $zoom
 * @param type $writemean
 */
function lstest_print_graphic($moduleid, $userid, $actualstudent, $coursemean, $activitymean, $totalmean, $zoom, $writemean) {
    global $CFG;
    echo "<center><img  border=\"1\" src=\"$CFG->wwwroot/mod/lstest/graph.php?id=$moduleid&sid=$userid&actualstudent=$actualstudent&coursemean=$coursemean&activitymean=$activitymean&totalmean=$totalmean&zoom=$zoom&writemean=$writemean\" alt=\"graph\"></center>";
}

/**
 * Print a form to control graph generation
 * @param type $url
 * @param type $moduleid
 * @param type $courseid
 * @param type $userid
 * @param boolean $actualstudent
 * @param boolean $coursemean
 * @param type $activitymean
 * @param type $totalmean
 * @param type $zoom
 * @param type $writemean
 * @param type $orderby
 */
function lstest_print_graphic_selector($url, $moduleid, $courseid, $userid, $actualstudent, $coursemean, $activitymean, $totalmean, $zoom, $writemean, $orderby = "items") {
    global $OUTPUT;

    if (!isset($actualstudent) && !isset($coursemean) && !isset($activitymean) && !isset($totalmean)) {
        $actualstudent = true;
        $coursemean = true;
    }
    echo "<BR>";
    echo "<FORM method='post' action='$url'>";
    echo "<CENTER>";
    if ($courseid > 1) {
        echo "<INPUT TYPE=checkbox NAME=actualstudent VALUE=true ";
        echo $actualstudent? 'checked' : '';
        echo ">" . get_string("actualstudent", "lstest") . " ";
        echo "<INPUT TYPE=checkbox NAME=activitymean VALUE=true ";
        echo $activitymean? 'checked' : '';
        echo ">" . get_string("activitymean", "lstest") . " ";
        echo "<INPUT TYPE=checkbox NAME=coursemean VALUE=true ";
        echo $coursemean? 'checked' : '';
        echo ">" . get_string("coursemean", "lstest") . " ";
    }
    else {
        echo "<INPUT TYPE=checkbox NAME=actualstudent VALUE=true ";
        echo $actualstudent? 'checked' : '';
        echo ">" . get_string("actualuser", "lstest") . " ";
    }
    echo "<INPUT TYPE=checkbox NAME=totalmean VALUE=true ";
    echo $totalmean? 'checked' : '';
    echo ">" . get_string("totalmean", "lstest") . " ";
    echo "<BR>";
    echo "<INPUT TYPE=checkbox NAME=zoom VALUE=true ";
    echo $zoom? 'checked' : '';
    echo ">" . get_string("zoom", "lstest") . " ";
    echo "<INPUT TYPE=checkbox NAME=writemean VALUE=true ";
    echo $writemean? 'checked' : '';
    echo ">" . get_string("writemeans", "lstest") . " ";
    echo "<INPUT TYPE=hidden NAME=orderby VALUE=$orderby > ";
    echo "<BR><BR>";
    echo "<INPUT TYPE=submit VALUE=\"" . get_string("redographic", "lstest") . "\">";
    echo $OUTPUT->help_icon('redographic', 'lstest');
    echo "</CENTER>";
    echo "</FORM>";
    echo "<BR><BR>";
}
?>
