<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../config.php');
require_once('locallib.php');

global $DB, $PAGE, $OUTPUT;

$courseid = optional_param('id', 0, PARAM_INT);        // Course ID

require_login($courseid);

$course = $DB->get_record('course', array('id' => $courseid));
if (!$course) {
    error('Course ID is incorrect');
}

add_to_log($course->id, 'lstest', 'view all', "index.php?id=$course->id", '');

$strstyles = get_string('modulenameplural', 'lstest');
$navlinks = array();
$navlinks[] = array('name' => $strstyles, 'link' => '', 'type' => 'activity');

$PAGE->set_title("$course->shortname: $strstyles");
$PAGE->set_heading($course->fullname);
$PAGE->set_url('/mod/lstest/editor/settings.php', array('course' => $courseid));
$PAGE->navbar->add($strstyles);
$PAGE->set_context(get_context_instance(CONTEXT_COURSE, $courseid));
$PAGE->set_pagelayout('incourse');

echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('modulenameplural', 'lstest'));

$tests = get_all_instances_in_course('lstest', $course);
if (!$tests) {
    notice("There are no tests", "../../course/view.php?id=$course->id");
    die;
}

$timenow = time();
$strname = get_string("name");
$strweek = get_string("week");
$strtopic = get_string("topic");

$table = new html_table();
if ($course->format == "weeks") {
    $table->head = array($strweek, $strname);
    $table->align = array("CENTER", "LEFT");
} else if ($course->format == "topics") {
    $table->head = array($strtopic, $strname);
    $table->align = array("CENTER", "LEFT", "LEFT", "LEFT");
} else {
    $table->head = array($strname);
    $table->align = array("LEFT", "LEFT", "LEFT");
}

foreach ($tests as $test) {
    $link = "<A HREF=\"view.php?id=$test->coursemodule\">$test->name</A>";

    if ($course->format == "weeks" or $course->format == "topics") {
        $table->data[] = array($test->section, $link);
    } else {
        $table->data[] = array($link);
    }
}

echo html_writer::table($table);

echo $OUTPUT->footer();
?>
