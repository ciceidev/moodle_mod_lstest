<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the backup steps that will be used by the backup_lstest_activity_task
 */

/**
 * Define the complete lstest structure for backup, with file and id annotations
 */
class backup_lstest_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        /*
         * Activity
         */

        // lstest activity instance
        $name = 'lstest';
        $attributes  = array(
            'id',
        );
        $elements =array(
            'name',
            'testsid',
            'intro',
            'introformat',
            'timecreated',
            'timemodified',
            'redoallowed',
        );
        $lstest = new backup_nested_element($name, $attributes, $elements);

        /*
         * Test definition
         */

        // test instance
        $name = 'test';
        $attributes = array(
            'id',
        );
        $elements = array(
            'name',
            'lang',
            'available',
            'redoallowed',
            'multipleanswer',
            'notansweredquestion',
        );
        $test = new backup_nested_element($name, $attributes, $elements);

        // test styles
        $styles = new backup_nested_element('styles');
        // nested style
        $name = 'style';
        $attributes = array(
            'id',
        );
        $elements = array(
            'name',
        );
        $nestedstyle = new backup_nested_element($name, $attributes, $elements);

        // test levels
        $levels = new backup_nested_element('levels');
        // nested level
        $name = 'level';
        $attributes = array(
            'id',
        );
        $elements = array(
            'name',
        );
        $nestedlevel = new backup_nested_element($name, $attributes, $elements);

        // test thresholds
        $thresholds = new backup_nested_element('thresholds');
        // nested threshold
        $name = 'threshold';
        $attributes = array(
            'id',
        );
        $elements = array(
            'stylesid',
            'levelsid',
            'infthreshold',
            'supthreshold',
        );
        $nestedthreshold = new backup_nested_element($name, $attributes, $elements);

        // test answers
        $answers = new backup_nested_element('answers');
        // nested answer
        $name = 'answer';
        $attributes = array(
            'id',
        );
        $elements = array(
            'name',
        );
        $nestedanswer = new backup_nested_element($name, $attributes, $elements);

        // test items
        $items = new backup_nested_element('items');
        // nested item
        $name = 'item';
        $attributes = array(
            'id',
        );
        $elements = array(
            'stylesid',
            'question',
        );
        $nesteditem = new backup_nested_element($name, $attributes, $elements);

        // test scores
        $scores = new backup_nested_element('scores');
        // nested score
        $name = 'score';
        $attributes = array(
            'id',
        );
        $elements = array(
            'itemsid',
            'answersid',
            'nocheckedscore',
            'checkedscore',
        );
        $nestedscore = new backup_nested_element($name, $attributes, $elements);

        /*
         * User scores and answers
         */

        // user scores
        $usersscores = new backup_nested_element('usersscores');
        // nested user scores
        $name = 'userscore';
        $attributes = array(
            'id',
        );
        $elements = array(
            'time',
            'userid',
            'stylesid',
            'levelsid',
            'score',
        );
        $nesteduserscore = new backup_nested_element($name, $attributes, $elements);

        // user answers
        $usersanswers = new backup_nested_element('usersanswers');
        // nested user answers
        $name = 'useranswer';
        $attributes = array(
            'id',
        );
        $elements = array(
            'time',
            'userid',
            'itemsid',
            'answersid',
            'checked',
        );
        $nesteduseranswer = new backup_nested_element($name, $attributes, $elements);

        // Build the tree
        $lstest->add_child($test);
        $test->add_child($styles);
        $styles->add_child($nestedstyle);
        $test->add_child($levels);
        $levels->add_child($nestedlevel);
        $test->add_child($thresholds);
        $thresholds->add_child($nestedthreshold);
        $test->add_child($answers);
        $answers->add_child($nestedanswer);
        $test->add_child($items);
        $items->add_child($nesteditem);
        $test->add_child($scores);
        $scores->add_child($nestedscore);

        $lstest->add_child($usersscores);
        $usersscores->add_child($nesteduserscore);

        $lstest->add_child($usersanswers);
        $usersanswers->add_child($nesteduseranswer);

        // Define sources
        $lstest->set_source_table('lstest', array('id' => backup::VAR_ACTIVITYID));
        $test->set_source_table('lstest_tests', array('id' => '../testsid'));
        $nestedstyle->set_source_table('lstest_styles', array('testsid' => '../../id'));
        $nestedlevel->set_source_table('lstest_levels', array('testsid' => '../../id'));
        $nestedanswer->set_source_table('lstest_answers', array('testsid' => '../../id'));
        // Looking for threshlods using stylesid
        $sql = '
            SELECT *
            FROM {lstest_thresholds}
            WHERE stylesid IN (
                SELECT id
                FROM {lstest_styles}
                WHERE testsid = ?)
            ';
        $params = array(
            '../../id'
        );
        $nestedthreshold->set_source_sql($sql, $params);
        // Looking for items using stylesid
        $sql = '
            SELECT *
            FROM {lstest_items}
            WHERE stylesid IN (
                SELECT id
                FROM {lstest_styles}
                WHERE testsid = ?)
            ';
        $params = array(
            '../../id'
        );
        $nesteditem->set_source_sql($sql, $params);
        // Looking for scores using answersid
        $sql = '
            SELECT *
            FROM {lstest_scores}
            WHERE answersid IN (
                SELECT id
                FROM {lstest_answers}
                WHERE testsid = ?)
            ';
        $params = array(
            '../../id'
        );
        $nestedscore->set_source_sql($sql, $params);

        // All the rest of elements only happen if we are including user info
        if ($userinfo) {
            $nesteduserscore->set_source_table('lstest_user_scores', array('lstestid' => backup::VAR_PARENTID));
            $nesteduseranswer->set_source_table('lstest_user_answers', array('lstestid' => backup::VAR_PARENTID));
        }

        // Define id annotations
        $nesteduserscore->annotate_ids('user', 'userid');

        // Define file annotations
        $lstest->annotate_files('mod_lstest', 'intro', null); // This file area hasn't itemid

        // Return the root element (lstest), wrapped into standard activity structure
        return $this->prepare_activity_structure($lstest);
    }
}
