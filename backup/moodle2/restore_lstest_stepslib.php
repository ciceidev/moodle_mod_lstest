<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package moodlecore
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_lstest_activity_task
 */

/**
 * Structure step to restore one lstest activity
 */
class restore_lstest_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('lstest', '/activity/lstest');
        $paths[] = new restore_path_element('lstest_tests', '/activity/lstest/test');
        $paths[] = new restore_path_element('lstest_styles', '/activity/lstest/test/styles/style');
        $paths[] = new restore_path_element('lstest_levels', '/activity/lstest/test/levels/level');
        $paths[] = new restore_path_element('lstest_thresholds', '/activity/lstest/test/thresholds/threshold');
        $paths[] = new restore_path_element('lstest_answers', '/activity/lstest/test/answers/answer');
        $paths[] = new restore_path_element('lstest_items', '/activity/lstest/test/items/item');
        $paths[] = new restore_path_element('lstest_scores', '/activity/lstest/test/scores/score');

        if ($userinfo) {
            $paths[] = new restore_path_element('lstest_user_scores', '/activity/lstest/usersscores/userscore');
            $paths[] = new restore_path_element('lstest_user_answers', '/activity/lstest/usersanswers/useranswer');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    /*
     * Lstest activity
     */

    protected function process_lstest($data) {
        global $DB;

        $data = (object)$data;

        $data->course = $this->get_courseid();
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the lstest record
        $newid = $DB->insert_record('lstest', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newid);
    }

    /*
     * Test definition
     */

    protected function process_lstest_tests($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->courseid = $this->get_courseid();

        $newid = $DB->insert_record('lstest_tests', $data);
        $this->set_mapping('lstest_tests', $oldid, $newid);
    }

    protected function process_lstest_styles($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $oldtestid = $this->get_old_parentid('lstest_tests');
        $data->testsid = $this->get_mappingid('lstest_tests', $oldtestid);

        $newid = $DB->insert_record('lstest_styles', $data);
        $this->set_mapping('lstest_styles', $oldid, $newid);
    }

    protected function process_lstest_levels($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $oldtestid = $this->get_old_parentid('lstest_tests');
        $data->testsid = $this->get_mappingid('lstest_tests', $oldtestid);

        $newid = $DB->insert_record('lstest_levels', $data);
        $this->set_mapping('lstest_levels', $oldid, $newid);
    }

    protected function process_lstest_thresholds($data) {
        global $DB;

        $data = (object)$data;

        $data->stylesid = $this->get_mappingid('lstest_styles', $data->stylesid);
        $data->levelsid = $this->get_mappingid('lstest_levels', $data->levelsid);

        $DB->insert_record('lstest_thresholds', $data);
    }

    protected function process_lstest_answers($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $oldtestid = $this->get_old_parentid('lstest_tests');
        $data->testsid = $this->get_mappingid('lstest_tests', $oldtestid);

        $newid = $DB->insert_record('lstest_answers', $data);
        $this->set_mapping('lstest_answers', $oldid, $newid);
    }

    protected function process_lstest_items($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $oldtestid = $this->get_old_parentid('lstest_tests');
        $data->testsid = $this->get_mappingid('lstest_tests', $oldtestid);
        $data->stylesid = $this->get_mappingid('lstest_styles', $data->stylesid);

        $newid = $DB->insert_record('lstest_items', $data);
        $this->set_mapping('lstest_items', $oldid, $newid);
    }

    protected function process_lstest_scores($data) {
        global $DB;

        $data = (object)$data;

        $data->itemsid = $this->get_mappingid('lstest_items', $data->itemsid);
        $data->answersid = $this->get_mappingid('lstest_answers', $data->answersid);

        $DB->insert_record('lstest_scores', $data);
    }

    /*
     * Users scores
     */

    protected function process_lstest_user_scores($data) {
        global $DB;

        $data = (object)$data;

        $data->time = $this->apply_date_offset($data->time);
        $data->lstestid = $this->get_new_parentid('lstest');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->stylesid = $this->get_mappingid('lstest_styles', $data->stylesid);
        $data->levelsid = $this->get_mappingid('lstest_levels', $data->levelsid);

        $DB->insert_record('lstest_user_scores', $data);
    }

    protected function process_lstest_user_answers($data) {
        global $DB;

        $data = (object)$data;

        $data->time = $this->apply_date_offset($data->time);
        $data->lstestid = $this->get_new_parentid('lstest');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->itemsid = $this->get_mappingid('lstest_items', $data->itemsid);
        $data->answersid = $this->get_mappingid('lstest_answers', $data->answersid);

        $DB->insert_record('lstest_user_answers', $data);
    }

    protected function after_execute() {
        global $DB;

        // Add lstest related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_lstest', 'intro', null);

        // Update lstestid in lstest activity instance
        // This could be done in process_lstest but if lstest its put after process_lstest_tests
        // get_mappingid don't get the new id of lstest_tests
        // maybe is something related to defined paths, cache or something like that
        // but in after_execute() we can read this information cleanly
        $lstest = $DB->get_record('lstest', array('id' => $this->get_new_parentid('lstest')));
        $lstest->testsid = $this->get_mappingid('lstest_tests', $lstest->testsid);
        $DB->update_record('lstest', $lstest);
    }
}
