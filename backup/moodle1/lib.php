<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * lstest conversion handler
 */
class moodle1_mod_lstest_handler extends moodle1_mod_handler {

    /** @var moodle1_file_manager */
    protected $fileman = null;

    /** @var int cmid */
    protected $moduleid = null;

    /**
     * Declare the paths in moodle.xml we are able to convert
     *
     * The method returns list of {@link convert_path} instances.
     * For each path returned, the corresponding conversion method must be
     * defined.
     *
     * Note that the path /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST does not
     * actually exist in the file. The last element with the module name was
     * appended by the moodle1_converter class.
     *
     * @return array of {@link convert_path} instances
     */
    public function get_paths() {
        return array(
            new convert_path(
                'lstest',
                '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST',
                array(
                    'newfields' => array('introformat' => FORMAT_HTML),
                )
            ),
            new convert_path('test', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST'),
            new convert_path('styles', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/STYLES'),
            new convert_path('style', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/STYLES/STYLE'),
            new convert_path('levels', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/LEVELS'),
            new convert_path('level', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/LEVELS/LEVEL'),
            new convert_path('thresholds', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/THRESHOLDS'),
            new convert_path('threshold', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/THRESHOLDS/THRESHOLD'),
            new convert_path('answers', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ANSWERS'),
            new convert_path('answer', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ANSWERS/ANSWER'),
            new convert_path('items', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ITEMS'),
            new convert_path('item', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ITEMS/ITEM'),
            new convert_path('scores', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/SCORES'),
            new convert_path('score', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/SCORES/SCORE'),
            // Userdata of this module
            // TODO: 24/06/2012 Moodle 2 core modules don't convert these info
            //new convert_path('usersscores', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSSCORES'),
            //new convert_path('userscore', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSSCORES/USERSCORE'),
            //new convert_path('usersanswers', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSANSWERS'),
            //new convert_path('useranswer', '/MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSANSWERS/USERANSWER'),
        );
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST
     * data available
     */
    public function process_lstest($data) {
        // get the course module id and context id
        $instanceid     = $data['id'];
        $cminfo         = $this->get_cminfo($instanceid);
        $this->moduleid = $cminfo['id'];
        $contextid      = $this->converter->get_contextid(CONTEXT_MODULE, $this->moduleid);

        // get a fresh new file manager for this instance
        $this->fileman = $this->converter->get_file_manager($contextid, 'mod_lstest');

        // convert course files embedded into the intro
        $this->fileman->filearea = 'intro';
        $this->fileman->itemid   = 0;
        $data['intro'] = moodle1_converter::migrate_referenced_files($data['intro'], $this->fileman);

        // start writing lstest.xml
        $this->open_xml_writer("activities/lstest_{$this->moduleid}/lstest.xml");

        // activity node
        $name = 'activity';
        $attributes = array(
            'id' => $instanceid,
            'moduleid' => $this->moduleid,
            'modulename' => 'lstest',
            'contextid' => $contextid
        );
        $this->xmlwriter->begin_tag($name, $attributes);

        // lstest node
        $name = 'lstest';
        $attributes = array(
            'id' => $instanceid,
        );
        $this->xmlwriter->begin_tag($name, $attributes);

        // lstest elements
        foreach ($data as $field => $value) {
            if ($field <> 'id') {
                $this->xmlwriter->full_tag($field, $value);
            }
        }

        return $data;
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST
     * data available
     */
    public function process_test($data) {
        // Start test node
        $name = 'test';
        $attributes = array(
            'id' => $data['id'],
        );
        $this->xmlwriter->begin_tag($name, $attributes);

        // Convert lang definitions with utf8 to lang whithout utf8.
        // Example: en_utf8 -> en
        if (isset($data['lang'])) {
            if (strripos($data['lang'], '_utf8')) {
                $data['lang'] = str_ireplace('_utf8', '', $data['lang']);
            }
        }

        // test elements
        foreach ($data as $field => $value) {
            if ($field <> 'id') {
                $this->xmlwriter->full_tag($field, $value);
            }
        }

        //$this->write_xml('option', $data, array('/option/id'));
    }

    /**
     * This is executed when the parser reaches the closing </TEST> element
     */
    public function on_test_end() {
        $this->xmlwriter->end_tag('test');
    }

    /**
     * This is executed when the parser reaches the <STYLES> opening element
     */
    public function on_styles_start() {
        $this->xmlwriter->begin_tag('styles');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/STYLES/STYLE
     * data available
     */
    public function process_style($data) {
        $this->write_xml('style', $data, array('/style/id'));
    }

    /**
     * This is executed when the parser reaches the closing </STYLES> element
     */
    public function on_styles_end() {
        $this->xmlwriter->end_tag('styles');
    }

    /**
     * This is executed when the parser reaches the <LEVELS> opening element
     */
    public function on_levels_start() {
        $this->xmlwriter->begin_tag('levels');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/LEVELS/LEVEL
     * data available
     */
    public function process_level($data) {
        $this->write_xml('level', $data, array('/level/id'));
    }

    /**
     * This is executed when the parser reaches the closing </LEVELS> element
     */
    public function on_levels_end() {
        $this->xmlwriter->end_tag('levels');
    }

    /**
     * This is executed when the parser reaches the <THRESHOLDS> opening element
     */
    public function on_thresholds_start() {
        $this->xmlwriter->begin_tag('thresholds');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/THRESHOLDS/THRESHOLD
     * data available
     */
    public function process_threshold($data) {
        $this->write_xml('threshold', $data, array('/threshold/id'));
    }

    /**
     * This is executed when the parser reaches the closing </THRESHOLDS> element
     */
    public function on_thresholds_end() {
        $this->xmlwriter->end_tag('thresholds');
    }

    /**
     * This is executed when the parser reaches the <ANSWERS> opening element
     */
    public function on_answers_start() {
        $this->xmlwriter->begin_tag('answers');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ANSWERS/ANSWER
     * data available
     */
    public function process_answer($data) {
        $this->write_xml('answer', $data, array('/answer/id'));
    }

    /**
     * This is executed when the parser reaches the closing </ANSWERS> element
     */
    public function on_answers_end() {
        $this->xmlwriter->end_tag('answers');
    }

    /**
     * This is executed when the parser reaches the <ITEMS> opening element
     */
    public function on_items_start() {
        $this->xmlwriter->begin_tag('items');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/ITEMS/ITEM
     * data available
     */
    public function process_item($data) {
        $this->write_xml('item', $data, array('/item/id'));
    }

    /**
     * This is executed when the parser reaches the closing </ITEMS> element
     */
    public function on_items_end() {
        $this->xmlwriter->end_tag('items');
    }

    /**
     * This is executed when the parser reaches the <SCORES> opening element
     */
    public function on_scores_start() {
        $this->xmlwriter->begin_tag('scores');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/TEST/SCORES/SCORE
     * data available
     */
    public function process_score($data) {
        $this->write_xml('score', $data, array('/score/id'));
    }

    /**
     * This is executed when the parser reaches the closing </SCORES> element
     */
    public function on_scores_end() {
        $this->xmlwriter->end_tag('scores');
    }

    /**
     * This is executed when the parser reaches the <USERSSCORES> opening element
     */
    public function on_usersscores_start() {
        $this->xmlwriter->begin_tag('usersscores');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSSCORES/USERSCORE
     * data available
     */
    public function process_userscore($data) {
        $this->write_xml('userscore', $data, array('/userscore/id'));
    }

    /**
     * This is executed when the parser reaches the closing </SCORES> element
     */
    public function on_usersscores_end() {
        $this->xmlwriter->end_tag('usersscores');
    }

    /**
     * This is executed when the parser reaches the <USERSANSWERS> opening element
     */
    public function on_usersanswers_start() {
        $this->xmlwriter->begin_tag('usersanswers');
    }

    /**
     * This is executed every time we have one /MOODLE_BACKUP/COURSE/MODULES/MOD/LSTEST/USERSANSWERS/USERANSWER
     * data available
     */
    public function process_useranswer($data) {
        $this->write_xml('useranswer', $data, array('/useranswer/id'));
    }

    /**
     * This is executed when the parser reaches the closing </USERSANSWERS> element
     */
    public function on_usersanswers_end() {
        $this->xmlwriter->end_tag('usersanswers');
    }

    /**
     * This is executed when we reach the closing </MOD> tag of our 'lstest' path
     */
    public function on_lstest_end() {
        // finalize lstest.xml
        $this->xmlwriter->end_tag('lstest');
        $this->xmlwriter->end_tag('activity');
        $this->close_xml_writer();

        // write inforef.xml
        $this->open_xml_writer("activities/lstest_{$this->moduleid}/inforef.xml");
        $this->xmlwriter->begin_tag('inforef');
        $this->xmlwriter->begin_tag('fileref');
        foreach ($this->fileman->get_fileids() as $fileid) {
            $this->write_xml('file', array('id' => $fileid));
        }
        $this->xmlwriter->end_tag('fileref');
        $this->xmlwriter->end_tag('inforef');
        $this->close_xml_writer();
    }
}
