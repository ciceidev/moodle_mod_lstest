<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $DB, $CFG;

require_once('../../config.php');
require_once('locallib.php');
require_once("$CFG->libdir/excellib.class.php");

$lstestid = required_param('lstestid', PARAM_INT);
$studentid = optional_param('userid', 0, PARAM_INT);

$lstest = $DB->get_record('lstest', array('id' => $lstestid));
$test = $DB->get_record('lstest_tests', array('id' => $lstest->testsid));
$course = $DB->get_record('course', array('id' => $lstest->course));

// Only admins and teachers can generate a report
require_login($course->id);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:viewstatistics', $context);

// Get required data
// Students
if (!$studentid) {
    $activitystudentsids = lstest_activity_students($lstestid);
}
else {
    $activitystudentsids = array($studentid);
}
// Styles
$styles = $DB->get_records('lstest_styles', array('testsid' => $test->id), 'id asc');
// Levels
$levels = $DB->get_records('lstest_levels', array('testsid' => $test->id), 'id asc');
$newlevels = array();
foreach ($levels as $level) {
    $newlevels[$level->id] = $level->name;
}
$levels = $newlevels;
// Answers
$answers = $DB->get_records('lstest_answers', array('testsid' => $test->id), 'id asc');
$newanswers = array();
foreach ($answers as $answer) {
    $newanswers[$answer->id] = $answer->name;
}
$answers = $newanswers;
// Questions (items)
$items = $DB->get_records('lstest_items', array('testsid' => $test->id), 'id asc');

// Filename to download
$timestamp = date('j-m-y', time());
//$timestamp = userdate(time());
$downloadfilename = clean_filename("$course->shortname $test->name $timestamp.xls");

// Create Excel workbook
$workbook = new MoodleExcelWorkbook("-");
$workbook->send($downloadfilename);

// Header format
$headerformat = & $workbook->add_format();
$headerformat->set_bold();
$headerformat->set_align('center');

// Row format
$rowformat = & $workbook->add_format();
$rowformat->set_align('center');

// Add first sheet (convert text encoding since Moodle library doesn't convert it)
$textlib = textlib_get_instance();
$sheetname = $textlib->convert(get_string('seestudents', 'lstest'), 'utf-8', 'windows-1252');
$worksheet = & $workbook->add_worksheet($sheetname);

// Rows to write in this sheet
$rows = array();

// Write first row with headers
$headers = array();
array_push($headers, get_string('name', 'moodle'));
// Score obtained in each style
foreach ($styles as $style) {
    array_push($headers, get_string('scorereport', 'lstest', $style->name));
}
// Level obtained in each style
foreach ($styles as $style) {
    array_push($headers, get_string('levelreport', 'lstest', $style->name));
}
array_push($headers, get_string('date', 'moodle'));
array_push($headers, get_string('course', 'moodle'));

array_push($rows, $headers);

if ($activitystudentsids) {
    foreach ($activitystudentsids as $userid) {
        $row = array();

        // User name
        $user = $DB->get_record('user', array('id' => $userid));
        array_push($row, "$user->lastname $user->firstname");

        // User scores by style
        $select = "lstestid = '$lstestid' AND userid = '$userid'";
        $userscores = $DB->get_records_select('lstest_user_scores', $select, null, 'stylesid asc');
        $firstscore = current($userscores);
        foreach ($userscores as $score) {
            array_push($row, $score->score);
        }

        // User levels by style
        foreach ($userscores as $score) {
            array_push($row, $levels[$score->levelsid]);
        }

        // timestamp
        array_push($row, userdate($firstscore->time));

        // course
        array_push($row, $course->shortname);

        array_push($rows, $row);
    }
}

// Write all rows of current sheet
foreach ($rows as $rowindex => $row) {
    foreach ($row as $columnindex => $column) {
        $worksheet->write($rowindex, $columnindex, $column, $rowindex < 1 ? $headerformat : $rowformat);
    }
}

/// Add second sheet (convert text encoding since Moodle library doesn't convert it)
$textlib = textlib_get_instance();
$sheetname = $textlib->convert(get_string('seeitemstatistic', 'lstest'), 'utf-8', 'windows-1252');
$worksheet = & $workbook->add_worksheet($sheetname);

// Rows to write in this sheet
$rows = array();

// Write first row with headers
$headers = array();
array_push($headers, get_string('name', 'moodle'));
// Questions
foreach ($items as $item) {
    array_push($headers, $item->question);
}
array_push($headers, get_string('date', 'moodle'));
array_push($headers, get_string('course', 'moodle'));

array_push($rows, $headers);

if ($activitystudentsids) {
    foreach ($activitystudentsids as $userid) {
        $row = array();

        // User name
        $user = $DB->get_record('user', array('id' => $userid));
        array_push($row, "$user->lastname $user->firstname");

        // User answers for each item
        foreach ($items as $item) {
            $select = "lstestid = '$lstestid' AND userid = '$userid' AND itemsid = '$item->id'";
            $useranswers = $DB->get_records_select('lstest_user_answers', $select, null, 'itemsid asc');
            $firstanswer = current($useranswers);
            $answerstr = '';
            foreach ($useranswers as $useranswer) {
                if ($useranswer->checked == '1') {
                    $answerstr .= $answers[$useranswer->answersid] . ', ';
                }
            }
            // Remove last trailing ', ' if needed
            if ($answerstr != '') {
                $answerstr = substr($answerstr, 0, strlen($answerstr) - 2);
            }
            array_push($row, $answerstr);
        }

        // timestamp
        array_push($row, userdate($firstanswer->time));

        // course
        array_push($row, $course->shortname);

        array_push($rows, $row);
    }
}

// Write all rows of current sheet
foreach ($rows as $rowindex => $row) {
    foreach ($row as $columnindex => $column) {
        $worksheet->write($rowindex, $columnindex, $column, $rowindex < 1 ? $headerformat : $rowformat);
    }
}

$workbook->close();

?>
