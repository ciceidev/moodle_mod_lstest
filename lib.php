<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Moodle modules required functions
 */

/**
* Saves a new instance of the lstest into the database
*
* Given an object containing all the necessary data,
* (defined by the form in mod_form.php) this function
* will create a new instance and return the id number
* of the new instance.
*
* @param object $test An object from the form in mod_form.php
* @param mod_lstest_mod_form $mform
* @return int The id of the newly inserted newmodule record
*/
function lstest_add_instance(stdClass $test, mod_lstest_mod_form $mform = null) {
    global $DB;

    $test->timecreated = time();
    $test->timemodified = $test->timecreated;

    return $DB->insert_record('lstest', $test);
}

/**
* Updates an instance of the lstest in the database
*
* Given an object containing all the necessary data,
* (defined by the form in mod_form.php) this function
* will update an existing instance with new data.
*
* @param object $test An object from the form in mod_form.php
* @param mod_lstest_mod_form $mform
* @return boolean Success/Fail
*/
function lstest_update_instance(stdClass $test, mod_lstest_mod_form $mform = null) {
    global $DB;

    $test->timemodified = time();
    $test->id = $test->instance;

    return $DB->update_record('lstest', $test);
}

/**
* Removes an instance of the lstest from the database
*
* Given an ID of an instance of this module,
* this function will permanently delete the instance
* and any data that depends on it.
*
* @param int $id Id of the module instance
* @return boolean Success/Failure
*/
function lstest_delete_instance($id) {
    global $DB;

    if (! $test = $DB->get_record('lstest', array('id' => $id))) {
        return false;
    }

    $result = true;

    if (! $DB->delete_records('lstest', array('id' => $test->id))) {
        $result = false;
    }

    // Delete also related test results
    $DB->delete_records('lstest_user_scores', array('lstestid' => $test->id));
    $DB->delete_records('lstest_user_answers', array('lstestid' => $test->id));

    return $result;
}

/**
 * Given a course ID, delete info associated to this course.
 * This deletes tests created into this course.
 * @param type $course
 * @param type $showfeedback
 */
function lstest_delete_course($course, $showfeedback) {
    global $DB;

    $coursetests = $DB->get_records('lstest_tests', array('courseid' => $course->id));
    if ($coursetests) {
        foreach ($coursetests as $test) {
            // Save this to delete after thresholds and scores
            $styles = $DB->get_records('lstest_styles', array('testsid' => $test->id));
            $items = $DB->get_records('lstest_items', array('testsid' => $test->id));

            $DB->delete_records('lstest_styles', array('testsid' => $test->id));
            $DB->delete_records('lstest_levels', array('testsid' => $test->id));
            $DB->delete_records('lstest_answers', array('testsid' => $test->id));
            $DB->delete_records('lstest_items', array('testsid' => $test->id));

            if ($styles) {
                foreach ($styles as $style) {
                    $DB->delete_records('lstest_thresholds', array('stylesid' => $style->id));
                }
            }
            if ($items) {
                foreach ($items as $item) {
                    $DB->delete_records('lstest_scores', array('itemsid' => $item->id));
                }
            }
        }
        $DB->delete_records('lstest_tests', array('courseid' => $course->id));
    }
}

function lstest_user_outline($course, $user, $mod, $test) {
/// Return a small object with summary information about what a
/// user has done with a given particular instance of this module
/// Used for user activity reports.
/// $return->time = the time they did it
/// $return->info = a short text description


    return NULL;
}

function lstest_user_complete($course, $user, $mod, $test) {
/// Print a detailed representation of what a  user has done with
/// a given particular instance of this module, for user activity reports.

    return true;
}

function lstest_print_recent_activity($course, $isteacher, $timestart) {
/// Given a course and a time, this module should find recent activity
/// that has occurred in styles activities and print it out.
/// Return true if there was output, or false is there was none.

    global $CFG;

    return false;  //  True if anything was printed, otherwise false
}

function lstest_cron () {
/// Function to be run periodically according to the moodle cron
/// This function searches for things that need to be done, such
/// as sending out mail, toggling flags etc ...

    global $CFG;

    return true;
}

/**
* Returns the information on whether the module supports a feature
*
* @see plugin_supports() in lib/moodlelib.php
* @param string $feature FEATURE_xx constant for requested feature
* @return mixed true if the feature is supported, null if unknown
*/
function lstest_supports($feature) {
    switch($feature) {
        case FEATURE_BACKUP_MOODLE2: return true;
        case FEATURE_MOD_INTRO: return true;
        case FEATURE_SHOW_DESCRIPTION: return true;
        default: return null;
    }
}

?>
