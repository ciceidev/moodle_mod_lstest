<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../config.php');
require_once('lib.php');

global $USER;

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}

require_login($course->id);

// Make sure this is a legitimate posting
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:taketest', $context);

if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error('Learning styles test ID was incorrect');
}

if (!$formdata = data_submitted()) {
    error('You are not supposed to use this script like that.');
}

$timenow = time();

$test = $DB->get_record('lstest_tests', array('id' => $lstest->testsid));
$styles = $DB->get_records('lstest_styles', array('testsid' => $test->id), 'id asc');
//$stylesnum = count($styles);
$items = $DB->get_records('lstest_items', array('testsid' => $test->id), 'id asc');
//$firstitem = current($items);
$answers = $DB->get_records('lstest_answers', array('testsid' => $test->id), 'id asc');

foreach ($styles as $style) {
    $result["$style->id"] = 0;
}

// Delete old user stored answers for this test instance
$DB->delete_records('lstest_user_answers', array('lstestid' => $lstest->id, 'userid' => $USER->id));

// Check a test with multiple answer
if ($test->multipleanswer) {
    foreach ($items as $item) {
        $stritemid = "itemid" . $item->id;
        foreach ($answers as $answer) {
            $stranswer = "answer" . $item->id . $answer->id;
            $score = $DB->get_record('lstest_scores', array('itemsid' => $_POST["$stritemid"], 'answersid' => $answer->id));

            if (!isset($_POST["$stranswer"])) {
                $_POST["$stranswer"] = 0;
            }

            if ($_POST["$stranswer"]) {
                $result["$item->stylesid"] += $score->checkedscore;
            } else {
                $result["$item->stylesid"] += $score->nocheckedscore;
            }

            $newdata2 = new stdClass();
            $newdata2->time = $timenow;
            $newdata2->lstestid = $lstest->id;
            $newdata2->userid = $USER->id;
            $newdata2->itemsid = $_POST["$stritemid"];
            $newdata2->answersid = $answer->id;
            $newdata2->checked = ( ($_POST["$stranswer"]) ? true : false );
            $DB->insert_record('lstest_user_answers', $newdata2);
        }
    }
}
// Check a test without multiple answers
else {
    foreach ($items as $item) {
        $stranswer = "answer" . $item->id;
        $stritemid = "itemid" . $item->id;
        $scores = $DB->get_records('lstest_scores', array('itemsid' => $_POST["$stritemid"]), 'id asc');

        if (!isset($_POST["$stranswer"])) {
            $_POST["$stranswer"] = 0;
        }

        foreach ($scores as $score) {
            if ($_POST["$stranswer"] == $score->answersid) {
                $result["$item->stylesid"] += $score->checkedscore;
            } else {
                $result["$item->stylesid"] += $score->nocheckedscore;
            }
        }

        foreach ($answers as $answer) {
            $newdata2 = new stdClass();
            $newdata2->time = $timenow;
            $newdata2->lstestid = $lstest->id;
            $newdata2->userid = $USER->id;
            $newdata2->itemsid = $_POST["$stritemid"];
            $newdata2->answersid = $answer->id;
            $newdata2->checked = ( ($answer->id == $_POST["$stranswer"]) ? true : false );
            $DB->insert_record('lstest_user_answers', $newdata2);
        }
    }
}

// Delete old user stored scores for this test instance
$DB->delete_records('lstest_user_scores', array('lstestid' => $lstest->id, 'userid' => $USER->id));

foreach ($styles as $style) {
    $thresholds = $DB->get_records('lstest_thresholds', array("stylesid" => $style->id), 'id asc');

    $newdata = new stdClass();

    foreach ($thresholds as $threshold) {
        if (($result["$style->id"] >= $threshold->infthreshold) &&
                ($result["$style->id"] <= $threshold->supthreshold)) {
            $newdata->levelsid = $threshold->levelsid;
        }
    }

    $newdata->time = $timenow;
    $newdata->lstestid = $lstest->id;
    $newdata->userid = $USER->id;
    $newdata->stylesid = $style->id;
    $newdata->score = $result["$style->id"];
    $DB->insert_record('lstest_user_scores', $newdata);
}

redirect($_SERVER["HTTP_REFERER"], get_string("changessaved"), 1);
exit;
?>
