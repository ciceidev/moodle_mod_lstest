<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Post installation procedure
 *
 * @see upgrade_plugins_modules()
 */
function xmldb_lstest_install() {
    // Import CHAEA TESTS on install
    global $CFG;
    require_once("$CFG->dirroot/mod/lstest/locallib.php");
    foreach (glob("$CFG->dirroot/mod/lstest/tests/examples/*.xml") as $xml_test_file) {
        lstest_import_test($xml_test_file, SITEID);
    }
    return true;
}

/**
 * Post installation recovery procedure
 *
 * @see upgrade_plugins_modules()
 */
function xmldb_lstest_install_recovery() {
}
