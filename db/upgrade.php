<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function xmldb_lstest_upgrade($oldversion=0) {
    global $DB;

    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

    // Moodle 1.9 upgrades

    // Update from old lstest with global scores to lstest per instance scores
    if ($oldversion < 2012052400) {
        // Make a backup of users results tables
        foreach (array('lstest_user_scores', 'lstest_user_answers') as $tablename) {
            $table = new xmldb_table($tablename);
            if ($dbman->table_exists($table)) {
                $newtablename = $tablename . '_bak';
                $newtable = new xmldb_table($newtablename);
                // Make a backup only if is not already one made
                if (!$dbman->table_exists($newtable)) {
                    $dbman->rename_table($table, $newtablename);
                }
            }
            else {
                false;
            }
        }

        // Recreate lstest_user_scores (it has a new field, lstestid)
        $table = new xmldb_table('lstest_user_scores');

        // Adding fields to table lstest_user_scores
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('lstestid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('stylesid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('levelsid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('score', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table lstest_user_scores
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Launch create table for lstest_user_scores
        $dbman->create_table($table);

        // Recreate lstest_user_answers (it has a new field, lstestid)
        $table = new xmldb_table('lstest_user_answers');

        // Adding fields to table lstest_user_answers
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('lstestid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('itemsid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('answersid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('checked', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');

        // Adding keys to table lstest_user_answers
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Launch create table for lstest_user_answers
        $dbman->create_table($table);

        // Add courseid field to lstest_tests
        $table = new xmldb_table('lstest_tests');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'notansweredquestion');
        $dbman->add_field($table, $field);

        // Add redoallowed field to lstest
        $table = new xmldb_table('lstest');
        $field = new xmldb_field('redoallowed', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'timemodified');
        $dbman->add_field($table, $field);

        // Associate global results to tests instances (duplicates the info for each instance)
        $tests = $DB->get_records('lstest_tests');
        if ($tests) {
            foreach ($tests as $test) {
                // Instances that use this test
                $lstests = $DB->get_records('lstest', array('testsid' => $test->id));
                if (!$lstests) {
                    $lstests = array();
                }
                // Styles
                $styles = $DB->get_records('lstest_styles', array('testsid' => $test->id));;
                // Levels
                $levels = $DB->get_records('lstest_levels', array('testsid' => $test->id));;
                // Answers
                $answers = $DB->get_records('lstest_answers', array('testsid' => $test->id));;
                // Items
                $items = $DB->get_records('lstest_items', array('testsid' => $test->id));;
                // Users that have made this test
                $users = array();
                foreach ($styles as $style) {
                    $user_scores = $DB->get_records('lstest_user_scores_bak', array('stylesid' => $style->id));
                    if ($user_scores) {
                        foreach ($user_scores as $user_score) {
                            array_push($users, $user_score->userid);
                        }
                    }
                }
                $users = array_unique($users);

                // Latests user scores and answers (get always one order by time desc)
                foreach ($users as $userid) {
                    foreach ($styles as $style) {
                        $select = "userid = '$userid' AND stylesid = '$style->id'";
                        $user_score = $DB->get_records_select('lstest_user_scores_bak', $select, null, 'time desc', '*', '0', '1');
                        if ($user_score) {
                            $user_score = current($user_score);
                            foreach ($lstests as $lstest) {
                                // Add test scores to test instance only if user is student in this course
                                $context = get_context_instance(CONTEXT_COURSE, $lstest->course);
                                if (is_enrolled($context, $userid)) {
                                    $user_score->lstestid = $lstest->id;
                                    $DB->insert_record('lstest_user_scores', $user_score);
                                }
                            }
                        }
                    }

                    foreach ($items as $item) {
                        foreach ($answers as $answer) {
                            $select = "userid = '$userid' AND itemsid = '$item->id' AND answersid = '$answer->id'";
                            $user_answer = $DB->get_records_select('lstest_user_answers_bak', $select, null, 'time desc', '*', '0', '1');
                            if ($user_answer) {
                                $user_answer = current($user_answer);
                                foreach ($lstests as $lstest) {
                                    // Add test answers to test instance only if user is student in this course
                                    $context = get_context_instance(CONTEXT_COURSE, $lstest->course);
                                    if (is_enrolled($context, $userid)) {
                                        $user_answer->lstestid = $lstest->id;
                                        $DB->insert_record('lstest_user_answers', $user_answer);
                                    }
                                }
                            }
                        }
                    }
                }

                // lstest redoallowed value should be equal to test redoallowed value
                // to be consistent with update of current tests
                foreach ($lstests as $lstest) {
                    $lstest->redoallowed = $test->redoallowed;
                    $DB->update_record('lstest', $lstest);
                }
            }
        }

        // Delete backup of lstest_user_scores and lstest_user_answers tables
        if ($result) {
            foreach (array('lstest_user_scores', 'lstest_user_answers') as $tablename) {
                $table = new xmldb_table($tablename . '_bak');
                if ($dbman->table_exists($table)) {
                    $dbman->drop_table($table);
                }
            }
        }

        upgrade_mod_savepoint(true, 2012052400, 'lstest');
    }

    // Update to Moodle 2

    if ($oldversion < 2012060700) {
        // Add introformat field to lstest and generate their values
        $table = new xmldb_table('lstest');
        $field = new xmldb_field('introformat', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'intro');

        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $lstests = $DB->get_records('lstest');
        if ($lstests) {
            foreach ($lstests as $lstest) {
                $lstest->introformat = FORMAT_HTML;
                $DB->update_record('lstest', $lstest);
            }
        }

        // Convert tests site courseid and languages with utf8 to Moodle 2 format
        $tests = $DB->get_records('lstest_tests', array('courseid' => 0));
        if ($tests) {
            foreach ($tests as $test) {
                // Convert lang definitions with utf8 to lang whithout utf8.
                // Example: en_utf8 -> en
                if (strripos($test->lang, '_utf8')) {
                    $test->lang = str_ireplace('_utf8', '', $test->lang);
                }
                // Convert all instances of tests->course = 0 to SITEID
                if ($test->courseid == 0) {
                    $test->courseid = SITEID;
                }
                $DB->update_record('lstest_tests', $test);
            }
        }

        upgrade_mod_savepoint(true, 2012060700, 'lstest');
    }

    return true;
}

?>
