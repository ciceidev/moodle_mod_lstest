<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG, $DB;

require_once('../../config.php');
require_once('locallib.php');
require_once('graphlib.php');
require_once("$CFG->libdir/graphlib.php");

// TODO: graph doesn't print if Moodle debugging is enabled
ini_set('display_errors', 0);
$olddebuglevel = $CFG->debug;
$CFG->debug = DEBUG_NONE;

$id = optional_param('id', 0, PARAM_INT);        // Course Module ID
$sid = optional_param('sid', 0, PARAM_INT);        // User ID
$actualstudent = optional_param('actualstudent', false, PARAM_BOOL);
$coursemean = optional_param('coursemean', false, PARAM_BOOL);
$activitymean = optional_param('activitymean', false, PARAM_BOOL);
$totalmean = optional_param('totalmean', false, PARAM_BOOL);
$zoom = optional_param('zoom', false, PARAM_BOOL);
$writemean = optional_param('writemean', false, PARAM_BOOL);

if ($id) {
    if (!$cm = $DB->get_record('course_modules', array('id' => $id))) {
        error("Course Module ID was incorrect");
    }

    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        error("Course is misconfigured");
    }

    if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
        error("Course module is incorrect");
    }
}

require_login($course->id);

$graph = new graph(650, 550);
$graph->x_data = array();
$graph->y_data = array();
$graph->y_tick_labels = array();
$graph->parameter['x_offset'] = 0;
$graph->parameter['path_to_fonts'] = "$CFG->dirroot/lib/";
$graph->parameter['lang_decode'] = '';
$graph->parameter['lang_transcode'] = strtolower(get_string('thischarset'));
$graph->parameter['legend'] = 'top-right';
$graph->parameter['legend_colour'] = 'black';       // legend text colour.
$graph->parameter['legend_border'] = 'none';        // legend border colour, or 'none'.
$graph->parameter['title'] = '';                    // text for graph title
$graph->parameter['inner_border'] = 'none';       // colour of border around actual graph, or 'none'.
$graph->parameter['y_ticks_colour'] = 'none';       // colour to draw y ticks, or 'none'
$graph->parameter['x_ticks_colour'] = 'none';       // colour to draw x ticks, or 'none'
$graph->parameter['y_grid'] = 'none';        // grid lines. set to 'line' or 'dash'...
$graph->parameter['x_grid'] = 'none';        //   or if set to 'none' print nothing.
$graph->parameter['axis_colour'] = 'none';      // colour of axis text.
$graph->parameter['y_axis_gridlines'] = 0;
$graph->parameter['x_axis_gridlines'] = 0;


$colour[0] = 'red';
$colour[1] = 'blue';
$colour[2] = 'lime';
$colour[3] = 'fuchsia';


$lstest->testsid = $lstest->testsid;
$styles = $DB->get_records('lstest_styles', array('testsid' => $lstest->testsid), 'id asc');
$stylesnum = count($styles);
$angleincrement = 2 * pi() / $stylesnum;
if ($zoom == true) {
    $axissize = ( $graph->parameter['width'] < $graph->parameter['height'] ? $graph->parameter['width'] / 2 : $graph->parameter['height'] / 2 ) - 3;
} else {
    $axissize = ( $graph->parameter['width'] < $graph->parameter['height'] ? $graph->parameter['width'] / 2 : $graph->parameter['height'] / 2 ) - 100;
}
$maxscore = -9000;
$minscore = 9000;
foreach ($styles as $style) {
    $thresholds = $DB->get_records('lstest_thresholds', array('stylesid' => $style->id));
    foreach ($thresholds as $threshold) {
        if ($maxscore < $threshold->supthreshold) {
            $maxscore = $threshold->supthreshold;
        }
        if ($minscore > $threshold->infthreshold) {
            $minscore = $threshold->infthreshold;
        }
    }
}

$spacebetweenticks = $axissize / ($maxscore - $minscore);

lstest_print_axis($lstest->testsid, $graph, $axissize, $minscore, $maxscore, $angleincrement, $spacebetweenticks);

lstest_write_names($lstest->testsid, $graph, $axissize, $angleincrement);

$scores = lstest_mean_scores($lstest->id, $lstest->testsid, $course->id);

if ($totalmean == true) {
    lstest_print_scores($lstest->testsid, $graph, $scores['all'], $axissize, $minscore, $maxscore, $angleincrement, $colour[3]);
    if ($writemean == true) {
        lstest_write_scores($lstest->testsid, $graph, $scores['all'], $minscore, $angleincrement, $spacebetweenticks, "4", $colour[3]);
    }
}

if (($cm->course > 1) && ($activitymean == true)) {
    lstest_print_scores($lstest->testsid, $graph, $scores['activity'], $axissize, $minscore, $maxscore, $angleincrement, $colour[2]);
    if ($writemean == true) {
        lstest_write_scores($lstest->testsid, $graph, $scores['activity'], $minscore, $angleincrement, $spacebetweenticks, "3", $colour[2]);
    }
}

if (($cm->course > 1) && ($coursemean == true)) {
    lstest_print_scores($lstest->testsid, $graph, $scores['course'], $axissize, $minscore, $maxscore, $angleincrement, $colour[1]);
    if ($writemean == true) {
        lstest_write_scores($lstest->testsid, $graph, $scores['course'], $minscore, $angleincrement, $spacebetweenticks, "2", $colour[1]);
    }
}

if ($actualstudent == true) {
    $stylescores = lstest_student_scores($lstest->id, $sid);
    lstest_print_scores($lstest->testsid, $graph, $stylescores, $axissize, $minscore, $maxscore, $angleincrement, $colour[0]);
    if ($writemean == true) {
        lstest_write_scores($lstest->testsid, $graph, $stylescores, $minscore, $angleincrement, $spacebetweenticks, "1", $colour[0]);
    } else {
        lstest_write_scores($lstest->testsid, $graph, $stylescores, $minscore, $angleincrement, $spacebetweenticks, "1", "black");
    }
}

lstest_print_legend($graph, $cm->course, $actualstudent, $coursemean, $activitymean, $totalmean, $colour, $maxscore);

$graph->draw();

ini_set('display_errors', 1);
$CFG->debug = $olddebuglevel;
?>
