<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $DB, $PAGE, $OUTPUT, $CFG;

require_once('../../config.php');
require_once('locallib.php');

$id = optional_param('id', 0, PARAM_INT);        // Course Module ID

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}
if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error('Course module is incorrect');
}

require_login($course->id);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:viewstatistics', $context);

add_to_log($course->id, "lstest", "view", "view.php?id=$cm->id", "$lstest->id");

$PAGE->set_title(format_string($lstest->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add(get_string('modulename', 'lstest'));
$PAGE->navbar->add(format_string($lstest->name));
$PAGE->set_url('/mod/lstest/stylestatistic.php', array('id' => $id));

echo $OUTPUT->header();

lstest_print_result_menu($course->id, $id, 'stylestatistic');

lstest_predominance_tables($lstest->id, $lstest->testsid, $course->id);

echo '<BR>';
echo $OUTPUT->footer();
?>


