<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $DB, $PAGE, $OUTPUT, $CFG;

require_once('../../config.php');
require_once('locallib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

if (!$cm = get_coursemodule_from_id('lstest', $id)) {
    error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    error('Course is misconfigured');
}
if (!$lstest = $DB->get_record('lstest', array('id' => $cm->instance))) {
    error('Course module is incorrect');
}

require_login($course->id);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/lstest:viewstatistics', $context);

add_to_log($course->id, "lstest", "view", "view.php?id=$cm->id", $lstest->id);

$PAGE->set_title(format_string($lstest->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add(get_string('modulename', 'lstest'));
$PAGE->navbar->add(format_string($lstest->name));
$PAGE->set_url('/mod/lstest/teststatistic.php', array('id' => $id));

echo $OUTPUT->header();

lstest_print_result_menu($course->id, $id, 'teststatistic');

$table = new stdClass();
$table->align = array("center", "center", "center", "center", "center", "center", "center", "center", "center", "center");

$activitystudentsids = lstest_activity_students($lstest->id);
$coursestudentsids = lstest_course_students($lstest->testsid, $course->id);
$allstudentsids = lstest_all_students($lstest->testsid);

$activitystudentsnum = count($activitystudentsids);
$coursestudentsnum = count($coursestudentsids);
$allstudentsnum = count($allstudentsids);

echo "<BR>";
$inthecoursestr = get_string("inthecourse", "lstest");
$intheactivitystr = get_string("intheactivity", "lstest");
$inmoodlestr = get_string("inmoodle", "lstest");
echo $OUTPUT->heading(get_string("numberofstudents", "lstest"));
$table->head = array($intheactivitystr, $inthecoursestr, $inmoodlestr);
$table->data = array();
$table->data[] = array($activitystudentsnum, $coursestudentsnum, $allstudentsnum);
lstest_print_table($table);


echo "<BR>";
$scores = lstest_mean_scores($lstest->id, $lstest->testsid, $course->id);
echo $OUTPUT->heading(get_string('testresults', 'lstest'));
$table->head = array(
    get_string('style', 'lstest'),
    get_string('activitymean', 'lstest'),
    get_string('coursemean', 'lstest'),
    get_string('totalmean', 'lstest')
);
$table->data = array();
$styles = $DB->get_records('lstest_styles', array('testsid' => $lstest->testsid), 'id asc');
foreach ($styles as $style) {
    $table->data[] = array($style->name, $scores['activity'][$style->id], $scores['course'][$style->id], $scores['all'][$style->id]);
}
lstest_print_table($table);


foreach ($styles as $style) {
    $activitymaxscores[$style->id] = -90000;
    $activityminscores[$style->id] = 90000;
    $coursemaxscores[$style->id] = -90000;
    $courseminscores[$style->id] = 90000;
    $totalmaxscores[$style->id] = -90000;
    $totalminscores[$style->id] = 90000;
}

foreach ($allstudentsids as $studentid) {
    $activityscores = lstest_student_scores($lstest->id, $studentid);
    $coursescores = lstest_student_scores_course($lstest->testsid, $course->id, $studentid);
    $allscores = lstest_student_scores_all($lstest->testsid, $studentid);

    foreach ($styles as $style) {
        // Platform max/min scores
        if ($allscores) {
            foreach ($allscores as $studentscores) {
                if ($studentscores[$style->id] > $totalmaxscores[$style->id]) {
                    $totalmaxscores[$style->id] = $studentscores[$style->id];
                }
                if ($studentscores[$style->id] < $totalminscores[$style->id]) {
                    $totalminscores[$style->id] = $studentscores[$style->id];
                }
            }
        }
        // Course max/min scores
        if ($coursescores) {
            foreach ($coursescores as $studentscores) {
                if (in_array($studentid, $coursestudentsids)) {
                    if ($studentscores[$style->id] > $coursemaxscores[$style->id]) {
                        $coursemaxscores[$style->id] = $studentscores[$style->id];
                    }
                    if ($studentscores[$style->id] < $courseminscores[$style->id]) {
                        $courseminscores[$style->id] = $studentscores[$style->id];
                    }
                }
            }
        }
        // Activity max/min scores
        if ($activityscores) {
            $studentscores = $activityscores;
            if (in_array($studentid, $activitystudentsids)) {
                if ($studentscores[$style->id] > $activitymaxscores[$style->id]) {
                    $activitymaxscores[$style->id] = $studentscores[$style->id];
                }
                if ($studentscores[$style->id] < $activityminscores[$style->id]) {
                    $activityminscores[$style->id] = $studentscores[$style->id];
                }
            }
        }
    }
}

foreach ($styles as $style) {
    if ($activitystudentsnum == 0) {
        $activitymaxscores[$style->id] = '';
        $activityminscores[$style->id] = '';
    }
    if ($coursestudentsnum == 0) {
        $coursemaxscores[$style->id] = '';
        $courseminscores[$style->id] = '';
    }
    if ($allstudentsnum == 0) {
        $totalmaxscores[$style->id] = '';
        $totalminscores[$style->id] = '';
    }
}

echo '<BR>';
echo $OUTPUT->heading_with_help(get_string("maxandminresults", "lstest"), "maxandminresults", "lstest");;
$table->headcolspan = array("1", "2", "2", "2");
$table->head = array(get_string("style", "lstest"), $intheactivitystr, $inthecoursestr, $inmoodlestr);
$table->data = array();
$maxscorestr = get_string("maxscore", "lstest");
$minscorestr = get_string("minscore", "lstest");
$table->data[0] = array("", "<FONT size=1>" . $maxscorestr . "</FONT>", "<FONT size=1>" . $minscorestr . "</FONT>", "<FONT size=1>" . $maxscorestr . "</FONT>", "<FONT size=1>" . $minscorestr . "</FONT>", "<FONT size=1>" . $maxscorestr . "</FONT>", "<FONT size=1>" . $minscorestr . "</FONT>");
foreach ($styles as $style) {
    $table->data[] = array(
        $style->name,
        $activitymaxscores[$style->id],
        $activityminscores[$style->id],
        $coursemaxscores[$style->id],
        $courseminscores[$style->id],
        $totalmaxscores[$style->id],
        $totalminscores[$style->id]
    );
}
lstest_print_table($table);

echo "<BR>";
echo $OUTPUT->footer();
?>


