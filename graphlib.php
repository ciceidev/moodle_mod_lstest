<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function lstest_print_axis($testid, $graph, $axissize, $minscore, $maxscore, $angleincrement, $spacebetweenticks) {
    global $DB;

    $ticksize = $axissize / 60;
    $angle = $angleincrement;

    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    $stylesnum = count($styles);

    foreach ($styles as $style) {

        $graph->line($graph->parameter['width'] / 2, $graph->parameter['height'] / 2, $graph->parameter['width'] / 2 + $axissize * cos($angle), $graph->parameter['height'] / 2 + $axissize * sin($angle), 'brush', 'square', 0, 'black', 0);
        //Dibujamos los ticks del eje
        $xtickposition = $graph->parameter['width'] / 2;
        $ytickposition = $graph->parameter['height'] / 2;
        if ($stylesnum == 2) {
            $graph->line($xtickposition - $ticksize * cos($angle + pi() / 2), $ytickposition - $ticksize * sin($angle + pi() / 2), $xtickposition + $ticksize * cos($angle + pi() / 2), $ytickposition + $ticksize * sin($angle + pi() / 2), 'brush', 'square', 0, 'black', 0);
        }
        for ($j = $minscore; $j < $maxscore; $j++) {
            $xtickposition += $spacebetweenticks * cos($angle);
            $ytickposition += $spacebetweenticks * sin($angle);
            $graph->line($xtickposition - $ticksize * cos($angle + pi() / 2), $ytickposition - $ticksize * sin($angle + pi() / 2), $xtickposition + $ticksize * cos($angle + pi() / 2), $ytickposition + $ticksize * sin($angle + pi() / 2), 'brush', 'square', 0, 'black', 0);
        }
        $angle -= $angleincrement;
    }
}

function lstest_write_names($testid, $graph, $axissize, $angleincrement) {
    global $DB;

    $message = lstest_create_message();

    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');

    $angle = $angleincrement;

    foreach ($styles as $style) {

        $xtickposition = $graph->parameter['width'] / 2 + $axissize * cos($angle);
        $ytickposition = $graph->parameter['height'] / 2 + $axissize * sin($angle);

        $realangle = abs($angle % (pi() * 2));

        if ((($realangle >= 0) && ($realangle <= pi() / 2)) || (($realangle > pi() / 2 * 3) && ($realangle < pi() * 2))) {
            $message['boundary_box']['x'] = $xtickposition + 10 * cos($angle);
            $message['boundary_box']['y'] = $graph->parameter['height'] - ($ytickposition + 10 * sin($angle));
        } elseif (($realangle > pi() / 2) && ($realangle <= pi() / 2 * 3)) {
            $message['boundary_box']['x'] = $xtickposition + 10 * cos($angle) - strlen($style->name) * 5.2;
            $message['boundary_box']['y'] = $graph->parameter['height'] - ($ytickposition + 10 * sin($angle));
        }

        $message['text'] = $style->name;
        $graph->print_TTF($message);
        $angle -= $angleincrement;
    }
}

function lstest_create_message() {
    $message['text'] = '';
    $message['points'] = 9;
    $message['angle'] = 0;
    $message['colour'] = 'black';
    $message['font'] = 'default.ttf';
    $message['boundary_box']['x'] = 0;
    $message['boundary_box']['y'] = 0;
    $message['boundary_box']['offsetX'] = 0;
    $message['boundary_box']['offsetY'] = 0;
    $message['boundary_box']['reference'] = 'left-top';
    $message['boundary_box']['height'] = 0;
    $message['boundary_box']['width'] = 0;
    return $message;
}

function lstest_print_scores($testid, $graph, $scores, $axissize, $minscore, $maxscore, $angleincrement, $colour) {
    global $DB;

    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    $counter = 0;
    foreach ($styles as $style) {
        $stylestointegers[$counter++] = $style->id;
    }

    $angle = $angleincrement;

    for ($i = 0; $i < $counter; $i++) {

        $result = ($maxscore - $minscore) * abs($scores[$stylestointegers[$i]] - $minscore) * cos($angle);
        if ($result != (float) 0) {
            $x1 = $axissize / ($maxscore - $minscore) * abs($scores[$stylestointegers[$i]] - $minscore) * cos($angle);
            //$x1 = $axissize/$result;
        } else {
            $x1 = 1;
        }

        $result = ($maxscore - $minscore) * abs($scores[$stylestointegers[$i]] - $minscore) * sin($angle);
        if ($result != (float) 0) {
            $y1 = $axissize / ($maxscore - $minscore) * abs($scores[$stylestointegers[$i]] - $minscore) * sin($angle);
            //$y1 = $axissize/$result;
        } else {
            $y1 = 1;
        }

        $result = ($maxscore - $minscore) * abs($scores[$stylestointegers[($i + 1) % $counter]] - $minscore) * cos($angle - $angleincrement);
        if ($result != (float) 0) {
            $x2 = $axissize / ($maxscore - $minscore) * abs($scores[$stylestointegers[($i + 1) % $counter]] - $minscore) * cos($angle - $angleincrement);
            //$x2 = $axissize/$result;
        } else {
            $x2 = 0;
        }

        $result = ($maxscore - $minscore) * abs($scores[$stylestointegers[($i + 1) % $counter]] - $minscore) * sin($angle - $angleincrement);
        if ($result != (float) 0) {
            $y2 = $axissize / ($maxscore - $minscore) * abs($scores[$stylestointegers[($i + 1) % $counter]] - $minscore) * sin($angle - $angleincrement);
            //$y2 = $axissize/$result;
        } else {
            $y2 = 0;
        }
        $graph->line($graph->parameter['width'] / 2 + $x1, $graph->parameter['height'] / 2 + $y1, $graph->parameter['width'] / 2 + $x2, $graph->parameter['height'] / 2 + $y2, 'brush', 'square', 0, $colour, 0);

        $angle -= $angleincrement;
    }
}

function lstest_write_scores($testid, $graph, $scores, $minscore, $angleincrement, $spacebetweenticks, $separation = "1", $colour = "black") {
    global $DB;

    $message = lstest_create_message();

    $angle = $angleincrement;

    $message['colour'] = $colour;

    $styles = $DB->get_records('lstest_styles', array('testsid' => $testid), 'id asc');
    foreach ($styles as $style) {

        $xtickposition = $graph->parameter['width'] / 2 + ($scores[$style->id] - $minscore) * $spacebetweenticks * cos($angle);
        $ytickposition = $graph->parameter['height'] / 2 + ($scores[$style->id] - $minscore) * $spacebetweenticks * sin($angle);

        $message['boundary_box']['x'] = $xtickposition - 10;
        $message['boundary_box']['y'] = $graph->parameter['height'] - ($ytickposition - $separation * 10);
        $message['text'] = $scores[$style->id];
        $graph->print_TTF($message);

        $angle -= $angleincrement;
    }
}

function lstest_print_legend(&$graph, $courseid, $actualstudent, $coursemean, $activitymean, $totalmean, $colour, $maxscore) {

    $graph->y_order = array();
    if ($actualstudent == true) {
        array_push($graph->y_order, 'actualstudent');
        if ($courseid > 1) {
            $graph->y_format['actualstudent'] = array('colour' => $colour[0], 'legend' => get_string("actualstudent", "lstest"));
        } else {
            $graph->y_format['actualstudent'] = array('colour' => $colour[0], 'legend' => get_string("actualuser", "lstest"));
        }
        $graph->y_data['actualstudent'] = array();
    }
    if (($coursemean == true) && ($courseid > 1)) {
        array_push($graph->y_order, 'coursemean');
        $graph->y_format['coursemean'] = array('colour' => $colour[1], 'legend' => get_string("coursemean", "lstest"));
        $graph->y_data['coursemean'] = array();
    }
    if (($activitymean == true) && ($courseid > 1)) {
        array_push($graph->y_order, 'activitymean');
        $graph->y_format['activitymean'] = array('colour' => $colour[2], 'legend' => get_string("activitymean", "lstest"));
        $graph->y_data['activitymean'] = array();
    }
    if ($totalmean == true) {
        array_push($graph->y_order, 'totalmean');
        $graph->y_format['totalmean'] = array('colour' => $colour[3], 'legend' => get_string("totalmean", "lstest"));
        $graph->y_data['totalmean'] = array();
    }
}
?>
