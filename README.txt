/**
 * Learning styles tests module definition
 *
 * @package    module
 * @subpackage lstest
 * @copyright  2008 CICEI http://http://www.cicei.com
 * @author     2008 Borja Rubio Reyes
 *             2012 Aday Talavera Hierro (Some improvements and update to Moodle 2.x)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

This module developed at CICEI (Innovation Center for Information Society) of
ULPGC (University of Las Palmas de Gran Canaria) allows to define and evaluate
learning styles tests. By default CHAEA (Cuestionario Honey-Alonso de Estilos de
Aprendizaje) (in Spanish, English and Deutsch) and Kolb/Honey-Mumford test (in Deutsch) is defined.

Main features:

* Allows interactive definition (from module administration and activity form) of
  new test through a sequence of pages that will ask for the neeeded information.

* Allows to export/import tests to/from a file in XML format.

* Allows students to take tests and view their results in a table and in a graph,
  comparing their scores with the activity, course and platform average results.

* Allows teachers to see the answers to each question and the results (compared
  with the three average results mentioned in the previous point) of all students
  in the course and to obtain various statistics (number of course students in which
  each style predominates, maximum and minimum results obtained, number of students
  who chose every possible answer for each question, ...).

* Allows teachers to generate an Excel file with information related to students
  pertenency to each style.

All texts and help messages in English (en), Spanish (es) and Deutsch (de).